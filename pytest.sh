#!/usr/bin/env bash

# Running pytest does not work due to the imported Fortran libraries
# and the intricacies of library imports of a single python call.
# As a workaround we run pytest individually for each test case.


for i in ./tests/test_*/test_*.py; do
    python3 -m pytest $i
    if [ $? -ne 0 ]
    then
        exit 1
    fi
done
exit 0
