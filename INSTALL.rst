Prerequisites
#############

In general, the kmos3 framework has been developed and tested on Ubuntu 20.04 in conjunction with
the ``apt`` packages

* ``gfortran``

* ``python3-dev``

* ``python3-tk``

and

* ``g++``

to compile the generated model-specific Fortran source code and include the python3 header files
and static library for the Python bindings. So things will most likely work best under a similar
setup.

For the installation it is recommended to set up a virtual environment with ``venv`` using the

* ``python3-venv``

package.

Installation
############

After obtaining the kmos3 source code by cloning or downloading this repository with ::

    cd $HOME
    git clone https://gitlab.mpcdf.mpg.de/fhi-theory/kmos3.git
    cd kmos3

set up and activate a virtual environment with ::

    python3 -m venv $HOME/kmos3_venv
    source $HOME/kmos3_venv/bin/activate

and install the kmos3 package with ::

    pip install .

or ::

    pip install .[complete]

Dependencies
############

Other than packages from the standard libraries the installation process will install

* ``setuptools`` < 60.0
* ``numpy`` < 1.23 (contains the Fortran to Python interface generator ``f2py``)
* ``lxml``
* ``ipython3``

and depending on the chosen extras (see ``setup.cfg``) some of the below packages:

* ``ase``
* ...

Test Input
##########

Running a minimal test case to check whether the installation was successful includes

Change to the examples directory ::

    cd examples

execute the Ziff-Gulari-Barshad (ZGB) model script ::

    python3 ZGB_model__build.py

hange to the export directory ::

    cd zgb_model_local_smart

and run the benchmark ::

    kmos3 benchmark

If everything is working you should see the single-core CPU time to run 10\ :sup:`6` steps printed
to STDOUT.

Getting Started
###############

Before creating any models of your own you might take look around the *examples* folder and try
to play around with the Ziff-Gulari-Barshad (ZGB) model already mentioned above. Instead of
``kmos3 benchmark``, try ::

    kmos3 view

within the directory and observe how it behaves for different conditions.

Create a model XML file as explained in the `tutorials`_, wich will give an XML
file that contains the entire definition of your KMC model. Run ::

  kmos3 export <xml-file>

and you will find a new folder under the same name with the compiled model and self-contained
source code. Inside that directory run ::

  kmos3 view

and readily watch your model and manipulate parameters at the same time.

For other ways of running models interactively or scripted please refer to the `tutorials`_.

.. _tutorials: https://kmos3.org/doc/tutorials
