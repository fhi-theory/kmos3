#!/usr/bin/env python3

import pytest
import os


@pytest.fixture
def move_to_directory(request):
    os.chdir(os.path.dirname(request.node.fspath))
