==========================
Frequently Asked Questions
==========================


What other KMC codes are there?
  Kinetic Monte Carlo codes that we are currently aware of, that are in some form released are
  with no claim of completeness:

  - `aKMC (EON) <https://theory.cm.utexas.edu/eon/akmc.html>`_ (G. Henkelman)
  - `MonteCoffee <https://montecoffee.readthedocs.io/en/latest/>`_ (M. Jørgensen and H. Grönbeck)
  - `CARLOS <https://carlos.win.tue.nl/>`_ (J. Lukkien)
  - `SPPARKS <https://www.sandia.gov/ccr/software/spparks/>`_ (S. Plimpton)
  - `Zacros <https://zacros.org/>`_ (M. Stamatakis)

What is the relation between kmos3, kmcos, and kmos?
  Initially, the code was named kmos for `kinetic modeling on steroids`. However, during the 2020
  revitalization and migration to python3, the code's name was changed kmcos with intention of
  greater emphasis of the generality of the code. In 2024 the name was then changed to kmos3
  due to a change of the maintainers.    
