.. title:: Overview

=======
|kmos3|
=======

.. |kmos3| image:: img/kmos3_logo.svg
   :alt: kmos3
   :width: 40%

The kmos3 software package is a vigorous attempt to make (lattice) kinetic
Monte Carlo (KMC) modelling more accessible. It is designed for and by KMC
model developers with the aim to facilitate and speed up KMC model development.

.. figure:: img/kmos3_code_structure.svg
   :width: 75%
   :align: center

   Code structure of the kmos3 software package.

Not sure about how and where to begin? Start with the
:ref:`API tutorial <api-tutorial>`.

.. toctree::
   :maxdepth: 0

   installation
   development
   tutorials/index
   topic_guides/index
   reference/index
   troubleshooting
   faq

This document was generated |date|.

.. |date| date:: %b %d, %Y
