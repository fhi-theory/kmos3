*********
Reference
*********

Command Line Interface (CLI)
============================

.. include:: cli.rst

Data Types
==========

kmos3.types
^^^^^^^^^^^

.. automodule:: kmos3.types
  :members:
  :undoc-members:
  :private-members:

.. .. autoclass:: kmos3.types.Project
..   :members:
..   :undoc-members:
..   :private-members:

.. .. autoclass:: kmos3.types.Meta
..   :members:
..   :undoc-members:
..   :private-members:

.. .. autoclass:: kmos3.types.Parameter
..   :members:
..   :undoc-members:
..   :private-members:

.. .. autoclass:: kmos3.types.LayerList
..   :members:
..   :undoc-members:
..   :private-members:

.. .. autoclass:: kmos3.types.Layer
..   :members:
..   :undoc-members:
..   :private-members:

.. .. autoclass:: kmos3.types.Site
..   :members:
..   :undoc-members:
..   :private-members:

.. .. autoclass:: kmos3.types.Species
..   :members:
..   :undoc-members:
..   :private-members:

.. .. autoclass:: kmos3.types.Process
..   :members:
..   :undoc-members:
..   :private-members:

.. .. autoclass:: kmos3.types.ConditionAction
..   :members:
..   :undoc-members:
..   :private-members:

.. .. autoclass:: kmos3.types.Coord
..   :members:
..   :undoc-members:
..   :private-members:

kmos3.io
^^^^^^^^

.. automodule:: kmos3.io
  :members:
  :undoc-members:
  :private-members:

.. .. autoclass:: kmos3.io.ProcListWriter
..   :members:
..   :undoc-members:
..   :private-members:

Runtime Frontend
================

kmos3.run
^^^^^^^^^

.. automodule:: kmos3.run
  :members:
  :undoc-members:
  :private-members:

.. autoclass:: kmos3.run.Model_Rate_Constants
  :members:
  :undoc-members:
  :private-members:
  :special-members: __call__

.. autoclass:: kmos3.run.Model_Parameters
  :members:
  :undoc-members:
  :private-members:
  :special-members: __call__

.. autoclass:: kmos3.run.ModelRunner
  :members:
  :undoc-members:
  :private-members:

.. autoclass:: kmos3.run.ModelParameter
  :members:
  :undoc-members:
  :private-members:

.. autoclass:: kmos3.run.PressureParameter
  :members:
  :undoc-members:
  :private-members:

.. autoclass:: kmos3.run.TemperatureParameter
  :members:
  :undoc-members:
  :private-members:

.. autoclass:: kmos3.run.LinearParameter
  :members:
  :undoc-members:
  :private-members:

.. autoclass:: kmos3.run.LogParameter
  :members:
  :undoc-members:
  :private-members:

kmos3.view
^^^^^^^^^^

.. automodule:: kmos3.view
  :members:
  :undoc-members:
  :private-members:

kmos3.cli
^^^^^^^^^

.. automodule:: kmos3.cli
  :members:
  :undoc-members:
  :private-members:

kmos3.utils
^^^^^^^^^^^

.. automodule:: kmos3.utils
  :members:
  :undoc-members:
  :private-members:

Connected Variables
===================

The connected_variables dictionary allows to pass string-writable objects created during the model
building into the runtime environment. This can be useful if a person needs access to some data
structures (like lists of surrounding sites) during runtime. Dictionaries, strings, and lists can
be passed. For more complex variables, one could pass the name of a pickle file. This feature is
used for the ``surroundingSitesDict``.

The basic syntax in a ``build_file`` would be as follows::

  kmc_model = kmos3.create_kmc_model(model_name)
  kmc_model.connected_variables['frog_list'] = [1,2,3,4]

Then, during runtime, one could do the following::

  print(model.connected_variables['frog_list'])

.. Currently, the way kmos3 processes things from the build file to the runtime environment is as
.. follows:

..   A person's build file makes a Project class object (typically "kmc_model"), for example in
..   https://gitlab.mpcdf.mpg.de/fhi-theory/kmos3/-/blob/main/examples/MyFirstDiffusion__build.py
..   That build file makes an xml file (or ini file), which occurs in types.py
..   ``_get_etree_xml`` or ``_get_etree_ini`` where a string is made that then gets written to
..   file.
..   That xml/ini is then read back in and validated , which occurs against a DTD. A new Project
..   class object is made from what is read back in.
..   It is important to recognize that the new Project class object has many attributes that are
..   the same as the one in the build file, but it is not the same object. It has fewer of the
..   original attributes due to hardcoded mapping during xml writing and xml reading.
..   When the source code compilation occurs, kmc_settings. is made. What is in kmc_settings
..   roughly mirrors the original Project class object, but it is actually from the new Project
..   class object that has been created from the xml.

kmos3 KMC Project DTD
=====================

The central storage and exchange format is XML. XML was chosen over JSON, pickle, or alike because
it seems to be one of the most flexible and universal format with good methods to define the
overall structure of the data.

One way to define an XML format is by using a document type description (DTD) and in fact at every
import a kmos3 file is validated against the DTD below.

.. literalinclude:: kmc_project_v0.5.dtd

Backends
========

In general, the backend includes all functions that are implemented in Fortran90, which therefore
should not have to be changed by hand often. The backend is divided into three modules, which
import each other in the following way ::

  base <- lattice <- proclist

The key for this division is reusability of the code. The `base` module implement all aspects of
the KMC code, which do not depend on the described model. Thus it "never" has to change. The
`latttice` module basically repeats all methods of the `base` model in terms of lattice
coordinates. Thus the `lattice` module only changes, when the geometry of the model changes, e.g.,
when you add or delete sites. The `proclist` module implements the process list, that is the
species or states each site can have and the elementary steps. Typically that changes most often
while developing a model.

The rate constants and physical parameters of the system are not implemented in the backend at
all, since in the physical sense they are too high-level to justify encoding and compilation at
the Fortran level and so they are typical read and parsed from a python script.

The `kmos3.run.KMC_Model` class implements a convenient interface for most of these functions,
however all public methods (in Fortran called subroutines) and variables can also be accessed
directly like so ::

  from kmos3.run import KMC_Model
  model = KMC_Model(print_rates=False, banner=False)
  model.base.<TAB>
  model.lattice.<TAB>
  model.proclist.<TAB>

which works best in conjunction with `ipython <ipython.org>`_.

local_smart
^^^^^^^^^^^

.. include:: robodoc/local_smart_base.rst
.. include:: robodoc/local_smart_lattice.rst
.. include:: robodoc/local_smart_proclist.rst
.. include:: robodoc/local_smart_kind_values.rst

lat_int
^^^^^^^

.. include:: robodoc/lat_int_base.rst
.. include:: robodoc/lat_int_lattice.rst
.. include:: robodoc/lat_int_proclist_constants.rst
.. include:: robodoc/lat_int_proclist.rst
.. include:: robodoc/lat_int_kind_values.rst

otf
^^^

.. include:: robodoc/otf_base.rst
.. include:: robodoc/otf_lattice.rst
.. include:: robodoc/otf_proclist_constants.rst
.. include:: robodoc/otf_proclist.rst
.. include:: robodoc/otf_kind_values.rst

.. TODO:: Fix some functions' docstrings.
