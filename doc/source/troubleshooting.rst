===============
Troubleshooting
===============

I found a bug or have a feature request. How can I get in touch?
    Please `create issues <https://gitlab.mpcdf.mpg.de/fhi-theory/kmos3/-/issues>`_ on our GitLab
    or send an email to kmos3.at.fhi.mpg.de.

My rate constant expression doesn't work. How can I debug it?
    When initializing the model, the backend uses
    ``kmos3.evaluate_rate_expression``. So you can try ::

        from kmos3 import evaluate_rate_expression
        evaluate_rate_expression('<your-string-here'>, parameters={})

    where parameters is a dictionary defining the variables that are used in the context of the
    expression evaluation, like so ::

        parameters = {
            'T': {'value': 500},
            'p_NClgas': {'value': 1},
            }

    Test only parts of your expression to localize the error. Typical mistakes are syntax errors
    (e.g., missing closing parentheses) and forgotten conversion factors (e.g., eV).

How can I get the chemical potential value that kmos3 is using internally?
    You can print the explicit value for specific conditions in the ``kmos3 shell``, for example
    like so ::

        from kmos3 import evaluate_rate_expression
        
        print(
            evaluate_rate_expression(
                'mu_COgas',
                {
                    'T': {'value': 600},
                    'p_COgas': {'value': 1}
                }
            )
        )

    where 'CO' should be replaced by whatever gas species you are inspecting. The resulting number
    is given in eV. Kmos3 linearly interpolates the gas phase chemical potential from the NIST
    JANAF thermochemical tables.

When I use ``kmos3 shell`` the model doesn't have the species and sites I have defined.
    Note that Fortran is case-insensitive. Therefore f2py turns all variable and function names
    into lowercase by convention. Try to lowercase your species or site name.

When running a model it prints `Warning: numerical precision too low, to resolve time-steps`.
    This means that the KMC step of the current process was so small compared to the current KMC
    time that for the processor :math:`t + \Delta t` is numericallly identical to :math:`t`. This
    should under normal circumstances only occur if you changed external conditions during a KMC
    run.

    Otherwise it could mean that your rate constants vary over 12 or more orders of magnitude. If
    this is the case one needs to think about whether non-coarse graind KMC is actually the right
    approach for this system. On the one hand because the selection of the next process will no
    longer be reliable and on the other hand because reasonable sampling of all involved processes
    may no longer happen.

When running a model without the GUI evaluation steps seem very slow.
    If you have a ``kmos3.run.KMC_Model`` instance and call ``model.get_atoms()`` the generation
    of the actual geometry takes the longest time. If you only have to evaluate coverages or
    turn-over frequencies you are better off using ``model.get_atoms(geometry=False)``, which
    returns an object with all numbers but without the geometry.

What units is kmos3 using?
    By default length is measured in Angstrom, energy in eV, pressure in bar, and constants are
    taken from CODATA 2010. Note that the rate expressions, though, contain explicit conversion
    factors like `bar`, `eV`, etc. If in doubt check the resulting rate constants by hand.

How can I change the occupation of a model at runtime?
    This is explained in detail in :ref:`manipulate_model_runtime`.

How can I quickly obtain k_tot?
    With the instantiated model, type ::

        model.base.get_accum_rate(model.proclist.nr_of_proc)
    
    where ``model.proclist.nr_of_proc`` is the total amount of processes.

How can I check the system size?
    You can check it with ::

        model.lattice.system_size

    to get the number of unit cells in the x, y, and z direction.
    The number of sites per unit cell is stored in ::

        model.lattice.spuck

    a. k. a. **s**\ites **p**\er **u**\nit **c**\ell **k**\onstant :-).
    Or you check ::

        model.base.get_volume()

    to get the total number of sites, i.e., ::

        model.base.get_volume() == model.lattice.system_size.prod()*model.lattice.spuck

.. todo:: Explain `post-mortem` procedure
