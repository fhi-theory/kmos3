A first kMC Model--with input files
===================================

One way to define kMC models is using an input file. The general format is identical
to the quite common ini-files. However, as we will see below, one can seamlessly embed
python code here to use more high-level constructs. But first things first.


Construct the model
^^^^^^^^^^^^^^^^^^^


The example sketched out here leads you to a kMC model for CO adsorption
and desorption on Pd(100) including a simple lateral interaction. Admittedly,
this hardly excites surface scientists but we have to start somewhere, right?


The following source code should be written into a text file using the editor of your choice. Use `.ini` as the suffix such as `myfirst_kmc.ini`. Start by filling out the meta information ::

    [Meta]
    author = 'Your name'
    email = 'your.name@server.com'
    model_name = 'MyFirstModel'
    model_dimension = 2

The important bit to notice here is that one uses the correct section names (e.g. `[Meta]`) and the case-sensitivity. Next we will add some species. Each species has its own section of the form `[Species <species_name>]` where `<species_name>` is a placeholder. Note the space between `Species` and the name, which shouldn't contain any spaces and follow the same rules as variables names. That is consisting only of letter and numbers or underscore (_) ::

    [Species empty]

    [Species CO]
    representation = Atoms('CO',[[0,0,0],[0,0,1.2]])

where the string passed as `representation` is a string representing
a CO molecule which can be evaluated in `ASE namespace <https://wiki.fysik.dtu.dk/ase/ase/atoms.html>`_.

Once you have all species declared is a good time to think about the geometry.
To keep it simple we will stick with a simple-cubic lattice in 2D, which could
for instance, represent the (100) surface of a fcc crystal with only
one adsorption site per unit cell. Begin by assigning a name to your layer ::

    [Layer simple_cubic]
    site hollow = (0.5, 0.5, 0.5)

Here we readily added a site named `'hollow` at the center of each unit cell.


Now you might be wondering about the absence of additional geometry details.
The reason is quite simple: the geometric location of a site is
meaningless from a kMC point of view. The master equation, which governs
the kMC process, is solely concerned with states and transitions between them.
Nevertheless, for visualization purposes, geometric information can be included,
as you have already done for the site. You can set the size of the unit cell
via ::

  [Lattice]
  cell_size = 3.5 3.5 10

which are prototypical dimensions for a single-crystal surface in
Angstrom.

Ok, let us see what we managed so far: you have a *lattice* with a
*site* that can be either *empty* or occupied with *CO*.


Populate process list and parameter list
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The remaining work is to populate the `process list` and the
`parameter list`. The parameter list defines the parameters
that can be used in the rate constant expressions.
In principle you could skip the parameter list and simply
hard code all parameters in the process list. However you will
miss out on some handy features like being able to easily change
parameters on-the-fly or even interactively.

A second benefit is that you achieve a clear separation
of the kinetic model from the barrier input,
which usually come from different sources.

In practice, filling both the parameter list and the process
list is often an iterative process. However, since our list 
is fairly short, we can try to set all parameters simultaneously.


Firstly, you'll need to define the external parameters 
that our model depends on. In this case, we consider 
the temperature and the CO partial pressure::

    [Parameter T]
    value = 600
    adjustable = True
    min = 400
    max = 600


and ::

    [Parameter p_CO]
    value = 1
    adjustable = True
    min = 1e-10
    max = 1e2

You can also set a default value along with a minimum and a maximum value.
These settings define the behavior of the scrollbars in the runtime GUI.

To describe the adsorption rate constant you will need the area
of the unit cell::

  [Parameter A]
  value = (3.5*angstrom)**2


Last but not least you need a binding energy of the particle on
the surface. Since we do not have an immediate value for the
gas phase chemical potential, we'll refer to it as deltaG and keep
it adjustable ::

  [Parameter deltaG]
  value = -0.5
  adjustable = True
  min = -1.3
  max = 0.3

Then you need to have at least two processes. A process or elementary step in kMC
entails a specific local configuration where an event can occur at a 
certain rate constant. In the framework here, this is articulated 
in terms of 'conditions' and 'actions'. [#proc_minilanguage]_
So for example an adsorption process requires at least one vacant site (condition). 
Only after fulfilling this condition can the site be occupied by CO (action) 
with a designated rate constant. Translated into code, this appears as follows ::

    [Process CO_adsorption]
    rate_constant = p_CO*bar*A/sqrt(2*pi*umass*m_CO/beta)
    conditions = empty@hollow
    actions = CO@hollow

.. note:: In order to ensure correct functioning of the kmos3 kMC solver, every action should have a corresponding condition for the same coordinate.

Now you might wonder, how come we can simply use m_CO and beta and such.
Well, that is because the evaluator will do some trickery to resolve such
terms. For example, beta is initially converted to 1/(kboltzmann*T) and as
long as you've defined a parameter `T` earlier, this will occur seamlessly. 
Similarly for m_CO, the evaluator looks up and includes the atomic masses. Note
that we need conversion factors of `bar` and `umass`.

Then the desorption process is almost the same, except the reverse::

    [Process CO_desorption]
    rate_constant = p_CO*bar*A/sqrt(2*pi*umass*m_CO/beta)*exp(beta*deltaG*eV)
    conditions = CO@hollow
    actions = empty@hollow


Finally save the file and run from the same directory ::

    kmos3 export myfrist_kmc.ini


If you now `cd` to that folder `myfirst_kmc` and run::

  kmos3 view

... and dada! Your first running kMC model right there!


If you're wondering why the CO molecules appear to be hanging in mid-air,
it's because you haven't set up the background yet.
Choose a transition metal of your choice and add it to the
lattice setup for extra credit :-).

Wondering where to go from here? If the work-flow makes
complete sense, you have a specific model in mind,
and just need some more idioms to implement it.
I suggest you take a look at the `examples folder <https://github.com/mhoffman/kmcos/tree/master/examples>`_.
for some hints. To learn more about the kmos3 approach
and methods you should into :ref:`topic guides <topic-guides>`.

Embedding python code [EXPERIMENTAL]
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If you start writing bigger model with sophisticated interaction writing down all processes in this .ini-format might be less than ideal. Therefore can understand embedded python code if you follow the following 2 rules: start every line containing python code with a `#%` (with a space after the `%`) and every variable in the .ini-parts that should be replaced by its python value in the current scope has to be placed in curly brackets {}. The latter is needed so that it gets interpolated by the str.format() function. To give you a simple example, let's add adsorption process for a lot of different species ::

    #@ for species in ['A', 'B', 'C', 'D']:
        [Process Adsorption_{species}]
        rate_constant = 100
        conditions = empty@hollow
        actions {species}@hollow

Of cource withespace matters here. To keep it simple avoid whitespace before the `#@` and indent the .ini-parts as if they were python code (counting whitespace from the `#` for the `#@` marker.


Taking it home
^^^^^^^^^^^^^^^

Despite its simplicity you have now seen all elements needed
to implement a kMC model. Hopefully, this has given you a foundational
understanding of the workflow.


.. [#proc_minilanguage]  You will have to describe all processes
                         in terms of  `conditions` and
                         `actions` and you find a more complete
                         description in the
                         :ref:`topic guide <proc_mini_language>`
                         to the process description syntax.

.. [#coord_minilanguage] The description of coordinates follows
                         the simple syntax of the coordinate
                         syntax and the
                         :ref:`topic guide <coord_mini_language>`
                         explains how that works.
