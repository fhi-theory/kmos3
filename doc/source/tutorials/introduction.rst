Introduction
^^^^^^^^^^^^

Kmos3 is designed for lattice based KMC simulations to understand chemical kinetics and
mechanisms. It has been used to produce a multitude of scientific publications. The best way to
learn how to use kmos3 is by following the examples.

If you have already followed the kmos3 installation instructions navigate to ``kmos3/examples``.
Inside the ``examples`` directory, run the following commands ::

    python3 MyFirstSnapshots__build.py
    cd MyFirstSnapshots_local_smart
    python3 runfile.py

The first command uses a python file to create a chemical model (process definitions) as well as
the corresponding KMC modeling executable. The ``local_smart`` dirctory suffix indicates the
default backend (default "KMC Engine", kmos3 has several). After the simulation has run, you will
see a csv file named ``MyFirstSnapshots_TOFs_and_Coverages.csv``. Open this file to see your first
KMC output!

Within the kmos3 package, a variety of examples exist and more features and a thorough tutorial
are under construction. In case of any questions, please contact us via kmos3.at.fhi.mpg.de.

Feature Overview
^^^^^^^^^^^^^^^^

.. automodule:: kmos3
