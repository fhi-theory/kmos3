The Runtime View
^^^^^^^^^^^^^^^^^

.. figure:: ../img/screenshots/screenshot_runtime_view.png
   :width: 100%
   :align: center

.. figure:: ../img/screenshots/methanation_combined.*
   :width: 100%
   :align: center

The compiled module can be run and watched in realtime.
When parameters are changed this is immediately reflected
in the rate constants.
