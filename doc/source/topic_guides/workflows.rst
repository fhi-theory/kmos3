Modelling Workflows
===================

At the core of modelling lies the art to capture the most important features of a system and leave
all others out. Kmos3 is designed around the fact that modeling is a creative and iterative
process.

A typical type of approach for modeling could be:

#. start with educated guess
#. calculate outcome
#. compare various observables and qualitative behavior with reference system
#. adapt model, go to 2. or publish model

So while this procedure is quite generic, it may help to illustrate that the chances to find and
capture the relevant features of a system are enhanced if the trial/learn loop is as short as
possible.

KMC Modeling
^^^^^^^^^^^^

A good way to define a model is to use a paper and pencil to draw your lattice, choose the species
that you will need, draw each process and write down an expression for each rate constant, and
finally fix all energy barriers and external parameters that you will need. Putting a model
prepared like this into a computer is a simple exercise. You enter a new model by filling in

    * meta information
    * lattice
    * species
    * parameters
    * processes

in roughly this order or adapt an existing one by opening a KMC XML file.

If you want to see the model run `kmos3 export <xml-file>`. You will get a subfolder with a
self-contained Fortran90 code, which represents the model and you can simply run
`kmos3 view` in this export folder as described in detail in the :ref:`API tutorial
<api-tutorial>`.

Kmos3 Workflows
^^^^^^^^^^^^^^^

Since kmos3 has several entry points, there are several ways of using it. This section will
outline different ways of using kmos3:

- *the render script*

  Just write complete scripts as outlined in :ref:`api-tutorial`. Compile source from there or
  inspect the XML file with one of the next methods below.

- *the CLI editor*

  Open an existing project XML file with ::

    kmos3 import <project_name>.xml

  and edit the project interactively on the ipython console.

- *edit the XML file*

  Just open the XML file of your kmos3 project with a text editor of your choice and inspect or
  edit your model right there. This might only be a last resort to figure out what is going on.
  XML is often not considered very readable and note that changing variable names in one place
  might cause inconsistencies in other places.
