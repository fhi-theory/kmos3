.. _topic-guides:

============
Topic Guides
============

The conceptual parts of this topic guide predate the
`kmos3 paper <https://doi.org/10.1016/j.cpc.2014.04.003>`_
(`arXiv <https://arxiv.org/abs/1401.5278>`_). Please refer to the paper for a thorough background
on KMC and lattice KMC on crystal surfaces. The more technical parts stated below might still be
useful for using kmos3.

.. toctree::
  :maxdepth: 2

  kmc_concept
  workflows
  kmos3_speed
  data_models
  coord_syntax
  proc_syntax
  lateral_interaction
  temporal_acceleration

..  Convergence Tests
..  The Editor Frontend
..  The Editor API
..  The Runtime Frontend
..  On the KMC solver
..  Nuts'n'Bolts of KMC
..  On the Random Number Generator
