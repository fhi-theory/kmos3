Temporal Acceleration
=====================

.. note::
  The temporal acceleration is still in an **experimental** state. Please report any bugs
  encountered.


This implementation of a temporal acceleration algorithm attempts to deal with the low barrier or
timescale disparity problem often encountered in KMC simulations. It is based on the acceleration
algorithm developed by Dybeck *et al.* [1]_ and the implementation in kmos3 is published by
Andersen *et al*. [2]_

In order for the scheme to work, it needs to be able to pair all processes into the corresponding
forward and reverse reactions. This is done according to the kmos3 actions and conditions, where
the forward process has the same actions as the conditions of the reverse process and vice versa.
For an example, take a look at `co_oxidation_ruo2_processes_paired__build.py` from the examples
folder.

To enable acceleration, compile the model with the command ``kmos3 export <model.xml> -t`` or if
using the backend for lateral interactions ``kmos3 export <model.xml> -b lat_int -t``.

.. note::
  The temporal acceleration currently does not work with the ``otf`` backend.

The above model has four adjustable parameters (c.f. article):

  - ``buffer_parameter`` (default: 1000): The smaller the value, the more aggresively the rate
    constants are scaled. Note that a good starting point is around the number of sites in the
    system.

  - ``sampling_steps`` (default: 20): The number of KMC steps to take between each reassessment of
    the scaling factors. This parameter seems neither to be important for the accuracy nor the
    efficiency of the code. The default value should be fine.

  - ``execution_steps`` (default: 200): The number of previous executions of either the forward or
    reverse process that is used to assess equilibrium. This parameter is also the number of
    executions of a forward/reverse process that must have occurred in the current superbasin for
    a process pair to be locally equilibrated. The default value seems to be close to the optimum
    efficiency for most systems tested so far.

  - ``threshold_parameter`` (default: 0.2): This parameter is used to assess whether a given
    process pair is equilibrated. The efficiency of the algorithm worsens considerably if going
    below the default value of 0.2, whereas the accuracy of the algorithm is typically not too
    sensitive to the exact value.

Overall, the ``buffer_parameter`` seems to be the most important parameter for the accuracy of the
algorithm, and one should always perform a careful convergence test with respect to this parameter
before trusting the results. In the limit of an infinite value for the ``buffer_parameter``, no
scaling of the rate constants will be done.

It is possible to set these four parameters either when initiating a model with ::

  model = KMC_Model(print_rates=False, banner=False, buffer_parameter=1000)

or one can use the set functions ::

  model.set_buffer_parameter(1000)

Get functions are also implemented ::

  model.get_buffer_parameter()

Note that if you change the ``execution_steps`` after initializing the model, the model will be
reset (as this parameter controls the length of some fortran arrays).

Accelerated KMC steps are run using the command ::

  model.do_acc_steps(nsteps)

In order to see what has happened during the simulations, one can use the commands ::

  model.print_scaling_stats()

or ::

  model.get_scaling_stats()

which print/return the names of the paired processes as well as the average used scaling factors
and the last set scaling factors (where the averages are typically higher (closer to 1), since
scaling factors are reset to 1 every time a non-equilibrated reaction is carried out). Further
implemented methods include ::

  model.print_scaling_factors

and ::

  model.print_proc_pair_eq

You can also use the command ::

  model.set_debug_level(value: 0, 1 or 2)

to activate printing of certain Fortran variables.

For models containing many different diffusion processes, the efficiency of the algorithm can be
significantly increased by considering these processes to be equilibrated (but not locally
equilibrated) by default. In practice this means that the execution of a diffusion process cannot
cause the unscaling of all processes, as is normally the case whenever a non-equilibrated reaction
occurs. However, diffusion processes still need to execute at least ``execution_steps`` times
within the superbasin before being labeled as locally equilibrated and possible being subject to
scaling. This above described behavior is default for any process containing the substring `diff`
in the process name.

.. [1] E. C. Dybeck, C. P. Plaisance, and M. Neurock,
       `J. Chem. Theory Comput. 13, 1525 (2017) <https://doi.org/10.1021/acs.jctc.6b00859>`_.

.. [2] M. Andersen, C. P. Plaisance, and K. Reuter,
       `J. Chem. Phys. 147, 152705 (2017) <https://doi.org/10.1063/1.4989511>`_.
