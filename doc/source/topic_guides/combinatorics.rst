Conquering Combinatorics with *itertools*
-----------------------------------------

Even restricting oneself to nearest neighbor lateral interactions or a number of different
configurations to be considered for lateral interactions can quickly lead to a couple of tens or
hundreds. A phenomenon which is among practitioners humbly referred to as *combinatorial
explosion*. Unfortunately, manually typing all these combinations is usually tiring and very error
prone. Fortunately, the ``itertools`` module from the python standard library allows to very
quickly generate all needed configurations. Before delving into the practical steps of this I
would like to point out that lateral interactions typically slow down the simulation by about
one order of magnitude, which is a purely empirical fact.
