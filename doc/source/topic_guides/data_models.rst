The kmos3 Data Model
====================

This section explains how kmos3 handles and represents a KMC model internally, which is important
to know if one wants to write new functionality.

The different functions and front-ends of kmos3 all interact in some way or another with instances
of the ``Project`` class. A ``Project`` instance is a representation of a KMC model. In a render
script you instantiate a ``Project`` and store it as an XML file, while reading in an XML file
instantiates a ``Project`` again. The XML file is validated by kmos3 and if you export source
code, kmos3 runs over the ``Project`` instance and creates the necessary Fortran 90 source code.

So the following things are in a Project:

- meta
- lattice (layers)
- species
- parameters
- processes

The language used here stems from modeling atomic movement on a fixed or evolving lattice like
structure. In a more general context one may rephrase them as :

- meta -> information about project
- lattice -> geometry
- species -> states
- parameters
- processes -> transitions
