Modeling Lateral Interactions
=============================

.. todo::
  This page needs more work.

Lateral Interaction Models
--------------------------

- pairwise interaction
- bond-order potentials

.. include:: combinatorics.rst
.. include:: otf_backend.rst
