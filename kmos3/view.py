#!/usr/bin/env python3

"""Run and view a kMC model. For this to work one needs a
kmc_model.(so/pyd) and a kmc_settings.py in the import path."""

#    Copyright 2009-2013 Max J. Hoffmann (mjhoffmann@gmail.com)
#    This file is part of kmos3.
#
#    kmos3 is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    kmos3 is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with kmos3.  If not, see <http://www.gnu.org/licenses/>.


import logging
import locale
import multiprocessing
import os
import platform
import tkinter as tk
import numpy as np
from kmos3.config import EXTENSIONS, APP_ABS_PATH
from kmos3.run import KMC_Model, get_tof_names, lattice, settings
if EXTENSIONS['ase']:
    from kmos3.extensions.ase_extension import KMC_ViewBox


# Hotfix for comma separated locales
# This leads to the slider returning floats with commas
# that are interpreted as strings and raise a TclError
if locale.getlocale() != ('en_US', 'UTF-8'):
    locale.setlocale(locale.LC_ALL, 'en_US.UTF-8')

logger = logging.getLogger(__name__)

try:
    import matplotlib
    import matplotlib.pyplot as plt
    if platform.system() == 'Linux':
        matplotlib.use('tkagg')
        from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg as FigureCanvasTk
    elif platform.system() == 'Windows':
        matplotlib.use('wxagg')
        from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigureCanvasTk
    elif platform.system() == 'Darwin':
        matplotlib.use('macosx')
        from matplotlib.backends.backend_macosx import FigureCanvasMac as FigureCanvasTk
    else:
        raise RuntimeError("OS type could not be determined.")
except ImportError as err:
    logger.warning(err)
    # raise RuntimeError("Could not import matplotlib frontend for plotting.")


class ParamSlider(tk.Scale):
    """A horizontal slider bar allowing the user to adjust a model parameter
    at runtime.
    """

    def __init__(self, master, name, value, xmin, xmax, scale_input, parameter_callback,
                 width=200, resolution=1000):
        self.settings = settings
        self.scale_input = scale_input
        if self.scale_input == 'linear':
            value = float(value)
            xmin = float(xmin)
            xmax = float(xmax)
        elif self.scale_input == 'log':
            value = round(np.log10(float(value)), 3)
            xmin = np.log10(float(xmin))
            xmax = np.log10(float(xmax))
        else:
            raise UserWarning("Unexpected scale mode %s" % self.scale_input)
        interval = xmax - xmin
        self.xmin = xmin
        self.xmax = xmax
        if not interval:
            self.xmax += 1.
            self.state = 'disabled'
        else:
            self.state = 'normal'
        super().__init__(master=master, orient='horizontal', length=width,
                         from_=self.xmin, to=self.xmax,
                         resolution=(self.xmax-self.xmin)/resolution)
        self.set(value)
        self.config(state=self.state)
        self.parameter_callback = parameter_callback
        self.param_name = name
        self.label = self.linlog_scale_format()
        self.master = master
        self.bind("<ButtonRelease-1>", self.value_changed)

    def linlog_scale_format(self):
        """Format a model parameter's name for display above
        the slider bar.
        """
        name = self.param_name
        unit = ''
        if self.param_name.endswith('gas'):
            name = name[:-3]
        if self.param_name.startswith('E_'):
            unit = '[eV]'
        elif self.param_name.startswith('p_'):
            unit = '[bar]'
        elif name == 'T':
            unit = '[K]'
        elif name == 'U':
            unit = '[V]'
        elif name == 'pH':
            unit = '[-]'
        if self.scale_input == 'log':
            vstr = '%s %s (log)' % \
                    (name, unit)
        elif self.scale_input == 'linear':
            vstr = '%s %s' % \
                    (name, unit)
        else:
            raise UserWarning("Unexpected scale mode %s" % self.scale_input)
        if self.state == 'disabled':
            vstr += ' (disabled)'
        return vstr

    def value_changed(self, event):
        """Handle the event, that slider bar has been dragged."""
        value = self.get()
        if value < self.xmin:
            value = self.xmin
        elif value > self.xmax:
            value = self.xmax
        if self.scale_input == 'log':
            value = 10**(value)
        self.parameter_callback(self.param_name, value)

class KMC_PlotBox():
    """
    The part of the viewer that displays the model's
    current coverage and turnover frequencies.
    """

    def __init__(self, master, image_queue):
        self.image_queue = image_queue
        self.master = master
        self.tofs = get_tof_names()
        self.times = []
        self.tof_hist = []
        self.y_max = []
        self.y_min = []
        # self.tof_integ_hist = []
        self.occupation_hist = []
        self.data_plot = plt.figure()
        self.tof_diagram = self.data_plot.add_subplot(211)
        self.tof_diagram.set_yscale('log')
        self.tof_plots = []
        for tof in self.tofs:
            self.tof_plots.append(self.tof_diagram.plot([], [], label=tof)[0])
        self.tof_diagram.legend(loc='lower left')
        self.tof_diagram.set_ylabel(
            'TOF in $\\mathrm{s}^{-1}\\mathrm{site}^{-1}$')
        self.occupation_plots = []
        self.occupation_diagram = self.data_plot.add_subplot(212)
        for species in sorted(settings.representations):
            self.occupation_plots.append(
                self.occupation_diagram.plot([], [], label=species)[0],)
        self.occupation_diagram.legend(loc=2)
        self.occupation_diagram.set_xlabel('$t$ in s')
        self.occupation_diagram.set_ylabel('Coverage')
        self.data_plot.tight_layout()
        self.width = self.master.winfo_width()
        self.height = self.master.winfo_height()
        self.plt_canvas = FigureCanvasTk(self.data_plot, master).get_tk_widget()
        self.plt_canvas.grid(row=0, column=0)
        self.plt_canvas.config(width=self.width, height=self.height)
        self.master.bind("<Configure>", self.on_resize)
        self.update_plots()

    def on_resize(self, event):
        """Update the canvas size."""
        if self.master.running:
            wscale = float(event.width)/self.width
            hscale = float(event.height)/self.height
            self.width = event.width
            self.height = event.height
            self.plt_canvas.config(width=self.width, height=self.height)
            self.plt_canvas.scale("all", 0, 0, wscale, hscale)

    def update_plots(self):
        """Update the coverage and TOF plots."""
        # fetch data piggy-backed on atoms object
        atoms = self.image_queue.get()
        new_time = atoms.kmc_time

        occupations = atoms.occupation.sum(axis=1) / lattice.spuck
        tof_data = atoms.tof_data
        # tof_integ_data = atoms.tof_integ

        # store locally
        while len(self.times) > getattr(settings, 'hist_length', 30):
            self.tof_hist.pop(0)
            self.y_max.pop(0)
            self.y_min.pop(0)
            # self.tof_integ_hist.pop(0)
            self.times.pop(0)
            self.occupation_hist.pop(0)

        self.times.append(new_time)
        self.tof_hist.append(tof_data)
        # self.tof_integ_hist.append(tof_integ_data)
        self.occupation_hist.append(occupations)

        # plot TOFs
        self.y_max.append(np.max(self.tof_hist))
        self.y_min.append(np.min(self.tof_hist))
        if self.y_max[-1] <= 0.0:
            self.y_max[-1] = 1e-5
        if self.y_min[-1] <= 0.0:
            self.y_min[-1] = self.y_max[-1] * 1e-4
        if len(self.times) > 1:
            for i, tof_plot in enumerate(self.tof_plots):
                tof_plot.set_xdata(self.times)
                tof_plot.set_ydata([tof[i] for tof in self.tof_hist])
            self.tof_diagram.set_xlim(self.times[0], self.times[-1])
            self.tof_diagram.set_ylim(self.y_min[-1]*1e-1, self.y_max[-1]*1e1)
            # plot occupation
            for i, occupation_plot in enumerate(self.occupation_plots):
                occupation_plot.set_xdata(self.times)
                occupation_plot.set_ydata(
                    [occ[i] for occ in self.occupation_hist])
            max_occ = max(occ[i] for occ in self.occupation_hist)
            self.occupation_diagram.set_ylim([0, max(1, max_occ)])
            self.occupation_diagram.set_xlim([self.times[0], self.times[-1]])

        self.data_plot.canvas.draw_idle()
        manager = plt.get_current_fig_manager()
        if hasattr(manager, 'toolbar'):
            toolbar = manager.toolbar
            if hasattr(toolbar, 'set_visible'):
                toolbar.set_visible(False)
        self.time = new_time
        self.master.after(1000, self.update_plots)

class AutoScrollbar(tk.Scrollbar):
    """
    A scroll bar that disappears/appears depending on
    whether the full content is visible.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
    def set(self, first, last):
        if float(first) <= 0.0 and float(last) >= 1.0:
            self.tk.call("grid", "remove", self)
        else:
            self.grid()
        tk.Scrollbar.set(self, first, last)

class KMC_Viewer():
    """
    A graphical front-end to run, manipulate
    and view a kMC model.
    """

    def __init__(self, signal_queue, parameter_queue, image_queue):
        # tkinter main window settings
        self.window = tk.Tk()
        self.window.iconphoto(False, tk.PhotoImage(
            file=os.path.join(APP_ABS_PATH, 'kmos3_logo.png')))
        height = 500
        plot_width = 600
        slider_width = 200
        scrl_width = 15
        control_width = slider_width + scrl_width
        width = plot_width + slider_width + scrl_width
        screen_width = self.window.winfo_screenwidth()
        screen_height = self.window.winfo_screenheight()
        x = (screen_width/2) - (width/2)
        y = (screen_height/2) - (height/2)
        self.window.geometry('%dx%d+%d+%d' % (width, height, x, y))
        self.window.minsize(width, height)
        self.window.maxsize(screen_width, screen_height)

        # Frame for the slider canvas and scrollbar
        self.frm_control = tk.Frame(master=self.window, height=height, width=control_width)
        self.frm_control.grid(row=0, column=0, sticky="nsew")
        # Frame for the coverage and tof plots
        self.frm_plot = tk.Frame(master=self.window, height=height)
        self.frm_plot.grid(row=0, column=1, sticky="nsew")

        self.slider_canvas = tk.Canvas(self.frm_control, height=height, width=slider_width)
        self.slider_canvas.grid(row=0, column=0, sticky="nsew")

        self.frm_slider = tk.Frame(master=self.slider_canvas, width=slider_width)

        self.vertscrbar = AutoScrollbar(master=self.frm_control, width=scrl_width,
                                        command=self.slider_canvas.yview)

        self.frm_control.rowconfigure(0, weight=1)

        self.window.protocol('WM_DELETE_WINDOW', self.exit)
        self.window.rowconfigure(0, minsize=height, weight=1)
        self.window.columnconfigure(0, minsize=control_width, weight=0)
        self.window.columnconfigure(1, minsize=plot_width, weight=1)

        self.parameter_queue = parameter_queue
        self.image_queue = image_queue
        self.signal_queue = signal_queue
        adjustable_params = [param for param in settings.parameters
                             if settings.parameters[param]['adjustable']]

        for i, param_name in enumerate(sorted(adjustable_params)):
            sub_frm = tk.Frame(self.frm_slider, width=slider_width-20)
            sub_frm.grid(row=i, column=0, sticky='nsew')
            param = settings.parameters[param_name]
            slider = ParamSlider(sub_frm, param_name, param['value'],
                                 param['min'], param['max'],
                                 param['scale'], self.parameter_callback,
                                 width=slider_width-25)
            slider.grid(row=0, column=0)
            text = tk.Label(sub_frm, text=slider.label)
            text.grid(row=1, column=0)


        self.slider_canvas.create_window(10, 10, anchor='nw', window=self.frm_slider)
        self.vertscrbar.grid(row=0, column=1, sticky="nsew")

        self.slider_canvas.update_idletasks()
        self.slider_canvas.config(
            scrollregion=(0, 0, control_width, self.frm_slider.bbox('all')[-1]+20),
            yscrollcommand=self.vertscrbar.set)

        self.frm_plot.running = True
        self.data_plot = KMC_PlotBox(self.frm_plot, self.image_queue)

        self.window.title('kmos3 viewer')
        self.window.mainloop()

    def parameter_callback(self, name, value):
        """Sent (updated) parameters to the model process."""
        settings.parameters[name]['value'] = str(value)
        self.parameter_queue.put(settings.parameters)

    def exit(self):
        """Exit the viewer application cleanly
        killing all subprocesses before the main
        process.
        """
        self.frm_plot.running = False
        self.signal_queue.put('STOP')
        self.window.quit()
        self.window.destroy()
        logger.info("kmos3 viewer terminated.")

def main(model=None, steps_per_frame=50000):
    """The entry point for the kmos3 viewer application. In order to
    run and view a model the corresponding kmc_settings.py and
    kmc_model.(so/pyd) must be in the current import path, e.g. ::

        from sys import path
        path.append('/path/to/model')
        from kmos3.view import main
        main() # launch viewer
    """

    logger.info("Starting kmos3 viewer...")
    if not hasattr(settings, 'tof_count'):
        logger.error("No 'tof_count' definition in the model. Terminating.")
        return
    image_queue = multiprocessing.Queue(maxsize=5)
    parameter_queue = multiprocessing.Queue(maxsize=50)
    signal_queue = multiprocessing.Queue(maxsize=10)

    if platform.system() in ['Linux', 'Darwin']:
        proc_view = multiprocessing.Process(target=KMC_Viewer, kwargs={
            'signal_queue': signal_queue,
            'parameter_queue': parameter_queue,
            'image_queue': image_queue})
        try:
            proc_view.start()
        except Exception as exc:
            print(exc)
            proc_view.kill()
        if EXTENSIONS['ase']:
            proc_ase = multiprocessing.Process(target=KMC_ViewBox,
                                               kwargs={'image_queue': image_queue})
            proc_ase.daemon = True
            try:
                proc_ase.start()
            except Exception as exc:
                print(exc)
                proc_ase.kill()
        model = KMC_Model(image_queue, parameter_queue, signal_queue,
                          steps_per_frame=steps_per_frame, banner=False)
        model.daemon = True
        try:
            model.start()
            proc_view.join()
        except Exception as exc:
            print(exc)
        finally:
            model.deallocate()
    elif platform.system() == 'Windows':
        logger.error("No Windows support (yet).")
    else:
        raise RuntimeError("OS type not supported.")
