#!/usr/bin/env python3

#    Copyright 2009-2013 Max J. Hoffmann (mjhoffmann@gmail.com)
#    This file is part of kmos3.
#
#    kmos3 is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    kmos3 is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with kmos3.  If not, see <http://www.gnu.org/licenses/>.

"""Entry point module for the command-line
   interface. The kmos3 executable should be
   on the program path, import this modules
   main function and run it.

   To call kmos3 commands as you would from the shell,
   use ::

       kmos3.cli.main('...')

   You may also use the syntax kmos3.export("...") for any cli command.
"""

import logging
import os


logger = logging.getLogger(__name__)

usage = {}

usage['benchmark'] = """Run several kMC steps on the model in the current directory
and report the runtime."""

usage['build'] = """Build 'kmc_model.\\*.%s' from the \\*.f90 files in the
current directory.""" % ('pyd' if os.name == 'nt' else 'so')

usage['export'] = """Take a kmos3 xml-file and export all generated
source code to the export-path. There try to
build the kmc_model.\\*.%s.""" % ('pyd' if os.name == 'nt' else 'so')

usage['settings-export'] = """Take a kmos3 xml-file and export 'kmc_settings.py'
to the export-path."""

usage['import'] = """Take a kmos3 xml-file and open an ipython shell
with the project_tree imported as kmc_model."""

usage['rebuild'] = """Export code and rebuild the binary module from
the 'kmc_model.\\*.%s' in the current directory.
""" % ('pyd' if os.name == 'nt' else 'so')

usage['shell'] = """Open an interactive shell and load the model
from the curent directory as KMC_Model in it"""

usage['view'] = """Take a 'kmc_model.\\*.%s' and 'kmc_settings.py' in the
same directory and start to simulate the
model visually.""" % ('pyd' if os.name == 'nt' else 'so')

usage['xml'] = """Print xml representation of model to stdout"""

def _benchmark(args):
    import sys
    import time
    from kmos3.run import KMC_Model
    sys.path.append(os.path.abspath(os.curdir))
    nsteps = args.nsteps
    model = KMC_Model(print_rates=False, banner=False)
    accelerated = model.can_accelerate
    if not accelerated:
        time0 = time.time()
        try:
            model.proclist.do_kmc_steps(nsteps)
        except AttributeError:  # kmos < 0.3 had no model.proclist.do_kmc_steps
            model.do_steps(nsteps)
        needed_time = time.time() - time0
        print('Using the [%s] backend.' % model.get_backend())
        print('%s steps took %.2f seconds' % (nsteps, needed_time))
        print('Or %.2e steps/s' % (1e6 / needed_time))
        model.deallocate()
    else:
        time0 = time.time()
        model.do_steps(nsteps)
        needed_time = time.time() - time0
        print('Using the [%s] backend.' % model.get_backend())
        print('Using the temporal acceleration scheme')
        print('%s normal steps took %.2f seconds' % (nsteps, needed_time))
        print('Or %.2e steps/s' % (1e6 / needed_time))
        print('')
        model.deallocate()
        model = KMC_Model(print_rates=False, banner=False)
        time0 = time.time()
        model.do_acc_steps(nsteps)
        needed_time = time.time() - time0
        print('%s accelerated steps took %.2f seconds' % (nsteps, needed_time))
        print('Or %.2e steps/s' % (1e6 / needed_time))
        model.deallocate()

def _build(args):
    from kmos3.utils import build
    build(args)

def _export(args):
    import glob
    import shutil
    import kmos3.types
    import kmos3.io
    if not args.outdir:
        outdir = '%s_%s' % (os.path.splitext(args.infile)[0], args.backend)
        logger.info("No export path provided. Exporting to '%s'.", outdir)
    else:
        outdir = os.path.abspath(args.outdir)

    infile = args.infile
    export_dir = os.path.join(outdir, 'src')

    project = kmos3.types.Project()
    project.import_file(infile)

    project.shorten_names(max_length=args.variable_length)

    kmos3.io.export_source(project,
                           export_dir,
                           options=args,
                           accelerated=args.temp_acc)
    if ((os.name == 'posix' and os.uname()[0] in ['Linux', 'Darwin'])
            or os.name == 'nt') and not args.source_only:
        os.chdir(export_dir)
        _build(args)
        for out in glob.glob('kmc_*'):
            if os.path.exists('../%s' % out):
                if args.overwrite:
                    overwrite = 'y'
                else:
                    overwrite = input(('Should I overwrite existing %s ?'
                                       '[y/N]  ') % out).lower()
                if overwrite.startswith('y'):
                    print('Overwriting {out}'.format(**locals()))
                    os.remove('../%s' % out)
                    shutil.move(out, '..')
                else:
                    print('Skipping {out}'.format(**locals()))
            else:
                shutil.move(out, '..')
        os.chdir('..')

def _settings_export(args):
    import kmos3.types
    import kmos3.io
    if not args.outdir:
        outdir = '%s_%s' % (os.path.splitext(args.infile)[0], args.backend)
        logger.info("No export path provided. Exporting to '%s'.", outdir)
    else:
        outdir = args.outdir
    infile = args.infile
    # export_dir = os.path.join(outdir, 'src')
    export_dir = outdir
    project = kmos3.types.Project()
    project.import_file(infile)
    writer = kmos3.io.ProcListWriter(project, export_dir)
    writer.write_settings()

def _import(args):
    import kmos3.types
    global project_tree
    infile = args.infile
    # kmc_model = kmos3.io.import_xml_file(infile)
    project_tree = kmos3.types.Project()
    project_tree.import_file(infile)
    shell(banner='Note: project_tree = kmos3.types.Project()\n'
                 '      project_tree.import_file(\'%s\')' % infile)

def _rebuild(args):
    # we could use _build but then we would have to rely on fortran files in the directory
    import glob
    import tempfile
    from kmos3.run import KMC_Model
    from kmos3.io import import_xml_file
    if not glob.glob('kmc_model.*.*'):
        raise Exception('No model binary found.')
    logger.warning('Rebuilding the model in the current directory.')
    model = KMC_Model(print_rates=False, banner=False)
    args.infile = tempfile.mktemp(suffix='.xml')
    with open(args.infile, 'w') as tmpf:
        tmpf.write(model.xml())
    project_tree = import_xml_file(args.infile)
    maxlen = 0
    for proc in project_tree.process_list:
        if len(proc.name) > maxlen:
            maxlen = len(proc.name)
    args.outdir = os.path.abspath(os.curdir)
    # Do not shorten process names (+4 comes from shorten_names function definition)
    args.variable_length = maxlen + 4
    # check if the model has been compiled using the temporal acceleration scheme
    if model.can_accelerate:
        args.temp_acc = True
    else:
        args.temp_acc = False
    # check the backend used to compile the model
    args.backend = model.get_backend()
    if hasattr(model, 'base_acf'):
        args.acf = True
    else:
        args.acf = False
    model.deallocate()
    # We want to rebuild it so overwrite and not source_only
    args.source_only = False
    args.overwrite = True
    try:
        _export(args)
    finally:
        os.remove(args.infile)

def _shell(args):
    import sys
    global model
    sys.path.append(os.path.abspath(os.curdir))
    from kmos3.run import KMC_Model
    try:
        model = KMC_Model(print_rates=False)
    except Exception as errormsg:
        print("Warning: could not import kmc_model! The error was: '{}'"
              "\n Please make sure you are in the right directory.".format(errormsg))
        raise

    shell(banner='Note: model = KMC_Model(print_rates=False)')
    try:
        model.deallocate()
    except:
        print("Warning: could not deallocate model. Was it allocated?")

def _view(args):
    import sys
    sys.path.append(os.path.abspath(os.curdir))
    from kmos3 import view
    view.main(steps_per_frame=args.steps_per_frame)

def _xml(args):
    import sys
    sys.path.append(os.path.abspath(os.curdir))
    from kmos3.run import KMC_Model
    model = KMC_Model(banner=False, print_rates=False)
    xml = model.xml()
    model.deallocate()
    print(xml)

def _parser():
    import argparse
    import kmos3

    parser = argparse.ArgumentParser(
        prog=kmos3.__name__,
        )
    parser.add_argument(
        '--version',
        action='version',
        version='%(prog)s '+kmos3.__version__,
        )

    subparsers = parser.add_subparsers(
        dest='parser',
        )

    # Common parser for the debug flag
    debug_parser = argparse.ArgumentParser(
        add_help=False,
        )
    debug_parser.add_argument(
        '-d', '--debug',
        action='store_true',
        help="turn on assertion statements in the F90 code (default: False)",
        )

    # Common parser for the backend flag
    backend_parser = argparse.ArgumentParser(
        add_help=False,
        )
    backend_parser.add_argument(
        '-b', '--backend',
        dest='backend',
        type=str,
        default='local_smart',
        choices=['local_smart', 'lat_int', 'otf'],
        help="choose the backend (default: 'local_smart') "
             "Note: 'lat_int' and 'otf' are EXPERIMENTAL",
        )

    # Common parser for the optimization flag
    optimize_parser = argparse.ArgumentParser(
        add_help=False,
        )
    optimize_parser.add_argument(
        '-n', '--no-compiler-optimization',
        dest='no_optimize',
        action='store_true',
        help="do not send optimizing flags to the compiler (default: False)",
        )
    # Let's try it witout the try...except
    # try:
    #     from numpy.distutils.fcompiler import get_default_fcompiler
    #     from numpy.distutils import log
    #     log.set_verbosity(-1, True)
    #     fcompiler = get_default_fcompiler()
    # except:
    #     fcompiler = 'gfortran'
    from numpy.distutils.fcompiler import get_default_fcompiler
    fcompiler = get_default_fcompiler()
    optimize_parser.add_argument(
        '-f', '--fcompiler',
        default=os.environ.get('F2PY_FCOMPILER', fcompiler),
        help="set the fortran compiler (by default it is determined from the environment "
             "variable 'F2PY_FCOMPILER' or, if not set, determined by numpy)",
        )

    # Common parser for compilation related flags
    compile_parser = argparse.ArgumentParser(
        add_help=False,
        parents=[debug_parser, backend_parser, optimize_parser],
        )
    compile_parser.add_argument(
        '-l', '--variable-length',
        dest='variable_length',
        default=95,
        type=int,
        help="set the maximum length for process names within the fortran source (default: 95)",
        )
    compile_parser.add_argument(
        '-t', '--temp-acc',
        dest='temp_acc',
        action='store_true',
        help="use the temporal acceleration scheme (default: False) "
             "Builds the modules base_acc.f90, lattice_acc.mpy, "
             "proclist_constants_acc.mpy and "
             "proclist_generic_subroutines_acc.mpy.",
        )
    compile_parser.add_argument(
        '--acf',
        action='store_true',
        help="build the modules base_acf.f90 and proclist_acf.f90 (default: False) "
             "The two modules contain functions to calculate the autocorrelation function "
             "(ACF) and mean squared displacement (MSD).",
        )
    compile_parser.add_argument(
        '-o', '--overwrite',
        action='store_true',
        help="overwrite 'kmc_settings.py' and 'kmc_model.\\*.%s' without confirmation "
             "(default: False)" % ('pyd' if os.name == 'nt' else 'so'),
        )

    # Common parser for the input
    in_parser = argparse.ArgumentParser(
        add_help=False,
        )
    in_parser.add_argument(
        'infile',
        type=str,
        help="XML or INI model file",
        )

    # Common parser for the output
    out_parser = argparse.ArgumentParser(
        add_help=False,
        )
    out_parser.add_argument(
        '--outdir',
        type=str,
        help="directory to build the kMC model",
        )

    # Put together the individual parsers
    benchmark_ = subparsers.add_parser(
        'benchmark',
        help=usage['benchmark'],
        )
    benchmark_.add_argument(
        '--nsteps',
        default=int(1e6),
        type=int,
        help="number of steps to run the model (default: 1e6)",
        )
    benchmark_.set_defaults(
        func=_benchmark,
        )

    build_ = subparsers.add_parser(
        'build',
        help=usage['build'],
        parents=[compile_parser],
        )
    build_.set_defaults(
        func=_build,
        )

    export_ = subparsers.add_parser(
        'export',
        help=usage['export'],
        parents=[compile_parser, in_parser, out_parser],
        )
    export_.add_argument(
        '-s', '--source-only',
        dest='source_only',
        action='store_true',
        help="export source only and don't build binary (default: False)",
        )
    export_.set_defaults(
        func=_export,
        )

    settings_export_ = subparsers.add_parser(
        'settings-export',
        help=usage['settings-export'],
        parents=[in_parser, out_parser, backend_parser],
        )
    settings_export_.set_defaults(
        func=_settings_export,
        )

    import_ = subparsers.add_parser(
        'import',
        help=usage['import'],
        parents=[in_parser],
        )
    import_.set_defaults(
        func=_import,
        )

    rebuild_ = subparsers.add_parser(
        'rebuild',
        help=usage['rebuild'],
        parents=[debug_parser, optimize_parser],
        )
    rebuild_.set_defaults(
        func=_rebuild,
        )

    shell_ = subparsers.add_parser(
        'shell',
        help=usage['shell'],
        aliases=['run'],
        )
    shell_.set_defaults(
        func=_shell,
        )

    view_ = subparsers.add_parser(
        'view',
        help=usage['view'],
        )
    view_.add_argument(
        '-v', '--steps-per-frame',
        dest='steps_per_frame',
        type=int,
        default='50000',
        help="number of steps per displayed frame (default: 50000)",
        )
    view_.set_defaults(
        func=_view,
        )

    xml_ = subparsers.add_parser(
        'xml',
        help=usage['xml'],
        )
    xml_.set_defaults(
        func=_xml,
        )
    return parser

def get_options(args=None, get_parser=False):
    """Get the default options and parse the
       the options provided on STDIN.
    """
    parser = _parser()
    if args:
        args = parser.parse_args(args)
    else:
        args = parser.parse_args()
    if not hasattr(args, 'func'):
        parser.print_help()
        parser.error("Please provide a valid command.")
    if get_parser:
        return args, parser
    return args

def main(args=None):
    """The CLI main entry point function.

    The optional argument args, can be used to
    directly supply command line argument like

    $ kmos3 <args>

    otherwise args will be taken from STDIN.
    """

    # global project_tree
    # global model
    if isinstance(args, str):
        args = args.split()
    args = get_options(args)
    flags = {x: vars(args)[x] for x in vars(args) if x not in ('parser', 'func')}
    if flags:
        logger.debug(
            "Runnung 'kmos3 %s' with options: %s",
            args.parser,
            flags,
            )
    else:
        logger.debug(
            "Runnung 'kmos3 %s'",
            args.parser,
            )
    args.func(args)

def shell(banner):
    """Wrapper around interactive ipython shell."""

    from IPython.terminal.embed import InteractiveShellEmbed
    InteractiveShellEmbed(banner1=banner)()
