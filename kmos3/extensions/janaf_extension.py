#!/usr/bin/env python3

"""
    This file implements functionality for
    the JANAF tables.
"""

#    This file is part of kmos3.
#
#    kmos3 is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    kmos3 is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with kmos3. If not, see <http://www.gnu.org/licenses/>.

import logging
import os
import numpy as np
from kmos3.units import kboltzmann_in_eVK, Jmol_in_eV
from kmos3.extensions import janaf_data

logger = logging.getLogger(__name__)


class GasSpecies():
    """
    Class to hold the gas species properties for the
    estimation of its chemical potential.
    """

    def __init__(self, janaf_file=None, **kwargs):
        super().__init__(**kwargs)
        if janaf_file:
            self.janaf_file = os.path.abspath(os.path.join(
                list(janaf_data.__path__)[0],
                janaf_file))
            self._read_janaf_file()
        else:
            self.temp_grid, self.free_nrg_grid, self.janaf_file = None, None, None

    def _read_janaf_file(self):
        try:
            data = np.loadtxt(self.janaf_file, skiprows=2, usecols=(0, 2, 4))
        except IOError:
            logger.warning("JANAF table '%s' not found.", self.janaf_file)
            self.temp_grid, self.free_nrg_grid, self.janaf_file = None, None, None
        else:
            self.temp_grid = data[:, 0]
            self.free_nrg_grid = (1000 * (data[:, 2] - data[0, 2]) -
                                  data[:, 0] * data[:, 1]) * Jmol_in_eV
        logger.debug("Read JANAF table in '%s'.", self.janaf_file)

    def get_chemical_potential(self, temperature, pressure):
        """
        Evaluate the gas phase chemical potential of a species
        at a given temperature and pressure from a grid of reference
        temperatures and free energies as in the JANAF tables.

        :param temperature: Temperature in K to calculate the chemical potential at
        :type temperature: float or int

        :param pressure: Pressure in bar to calculate the chemical potential at
        :type pressure: float or int

        :return: The chemical potential
        :rtype: float

        :Example:
            >>> CO = GasSpecies()
            >>> CO.get_chemical_potential(273.15, 1.0)
            TODO
        """
        if self.janaf_file:
            # interpolate given grid
            return np.interp(temperature, self.temp_grid, self.free_nrg_grid) + \
                kboltzmann_in_eVK * temperature * np.log(pressure)
        logger.warning("No JANAF table. Using a chemical potential of zero.")
        return 0.0

gas_species = {
    'H2': {
        'janaf_file': 'H-050.txt',
    },
    'CH4': {
        'janaf_file': 'C-067.txt',
    },
    'O2': {
        'janaf_file': 'O-029.txt',
    },
    'NO': {
        'janaf_file': 'N-005.txt',
    },
    'NO2': {
        'janaf_file': 'N-007.txt',
    },
    'NO3': {
        'janaf_file': 'N-009.txt',
    },
    'CO': {
        'janaf_file': 'C-093.txt',
    },
    'CO2': {
        'janaf_file': 'C-095.txt',
    },
    'NH3': {
        'janaf_file': 'H-083.txt',
    },
    'C2H4': {
        'janaf_file': 'C-128.txt',
    },
    'HCl': {
        'janaf_file': 'Cl-026.txt',
    },
    'Cl2': {
        'janaf_file': 'Cl-073.txt',
    },
    'H2O': {
        'janaf_file': 'H-064.txt',
    },
}

liquid_species = {
    'H2O': {
        'janaf_file': 'H-063.txt',
    },
}
