#!/usr/bin/env python3

"""
    This file implements functionality only available if
    the Atomic Simulation Environment (ase) is installed.
"""

#    This file is part of kmos3.
#
#    kmos3 is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    kmos3 is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with kmos3. If not, see <http://www.gnu.org/licenses/>.

import logging
import os
import tkinter as tk
import numpy as np
import ase
from ase.io.png import PNG
from ase.build import molecule
from ase.thermochemistry import HarmonicThermo, IdealGasThermo #, HinderedThermo
from ase.gui.gui import GUI
from ase.gui.ui import MenuItem as M
from kmos3.config import APP_ABS_PATH
from kmos3.units import cm_to_eV, bar

logger = logging.getLogger(__name__)


def get_mass(name):
    """
    Calculate the mass of a species in atomic mass units.

    :param name: Chemical formula of the species
    :type name: str

    :return: The mass in atomic mass units
    :rtype: float

    :Example:
        >>> get_mass('CO')
        28.009999999999998
    """

    symbols = ase.symbols.string2symbols(name)
    numbers = [ase.data.atomic_numbers[symbol] for symbol in symbols]
    masses = [ase.data.atomic_masses[number] for number in numbers]
    return sum(masses)

class GasSpecies():
    """
    Class to hold the gas species properties for the
    estimation of its free energy.
    """

    def __init__(self, name='', atoms=None, symmetrynumber=None, geometry=None,
                 spin=None, frequencies=None, freq_unit='cm-1', **kwargs):
        super().__init__(**kwargs)
        self.name = name
        self.atoms = atoms
        self.symmetrynumber = symmetrynumber
        self.geometry = geometry
        self.spin = spin
        if freq_unit == 'cm-1':
            self.frequencies = [i * cm_to_eV for i in frequencies]
        elif freq_unit == 'eV':
            self.frequencies = frequencies
        else:
            raise Exception("Unknown unit of the provided frequencies.")

    def get_ideal_gas_gibbs_free_energy(self, potentialenergy, temperature, pressure,
                                        pressure_unit='bar'):
        """
        Evaluate the Gibbs free energy of an gas species at a given temperature
        and pressure.

        :param potentialenergy: The potential energy in eV
        :type potentialenergy: float or int

        :param frequencies: The vibrational energies of the gas species in cm-1 or eV.
        :type frequencies: list of floats or ints

        :param temperature: Temperature in K to calculate the Gibbs free energy at
        :type temperature: float or int

        :param pressure: Pressure to calculate the Gibbs free energy at
        :type pressure: float or int

        :param pressure_unit: Pressure unit
        :type pressure_unit: str, 'Pa' or 'bar', default: 'Pa'

        :return: Gibbs free energy
        :rtype: float

        :Example:
            >>> CO = GasSpecies(name='CO', **gas_species['CO'])
            >>> CO.get_ideal_gas_gibbs_free_energy(potentialenergy=0.0,
                                                   pressure=101325.0,
                                                   temperature=298.15)
            -0.3868572090260546
        """

        if not self.atoms:
            try:
                self.atoms = molecule(self.name)
                logger.info("Created an ase.Atoms object from the provided name "
                            "'%s' to calculate the moments of inertia and molecular mass.",
                            self.name)
            except Exception as exc:
                raise RuntimeError(exc)
        if pressure_unit == 'bar':
            pressure *= bar
        elif pressure_unit == 'Pa':
            pass
        else:
            raise Exception("Unknown unit for the provided pressure.")
        thermo_gas = IdealGasThermo(
            vib_energies=self.frequencies,
            potentialenergy=potentialenergy,
            atoms=self.atoms,
            geometry=self.geometry,
            symmetrynumber=self.symmetrynumber,
            spin=self.spin
            )
        return thermo_gas.get_gibbs_energy(temperature=temperature, pressure=pressure,
                                           verbose=False)

class AdsorbateSpecies():
    """
    Class to estimate the adsorbte species free energy.
    """

    def __init__(self, name='', **kwargs):
        super().__init__(**kwargs)
        self.name = name

    @staticmethod
    def get_harmonic_adsorbate_gibbs_free_energy(potentialenergy, frequencies, temperature,
                                                 freq_unit='cm-1'):
        """
        Evaluate the Gibbs free energy of an adsorbed species in the
        harmonic limit at a given temperature.

        :param potentialenergy: The potential energy in eV
        :type potentialenergy: float or int

        :param frequencies: The vibrational energies of the adsorbed species in cm-1 or eV.
            Note that frequencies should have a lower bound, e.g., 56 cm-1, in order to bound
            entropic contributions
        :type frequencies: list of floats or ints

        :param temperature: Temperature in K to calculate the Gibbs free energy at
        :type temperature: float or int

        :param freq_unit: Vibrational energies unit
        :type freq_unit: str, 'cm-1' or 'eV', default: 'cm-1'

        :return: Gibbs free energy. Precisely speaking, the Helmholtz free energy is returned.
            However, the Gibbs free energy is the Helmholtz free energy plus a pV term,
            which is usually neglected as it is assumed to be small
        :rtype: float

        :Example:
            >>> kmos3.species.AdsorbateSpecies.get_harmonic_adsorbate_gibbs_free_energy(
                    potentialenergy=0.0,
                    frequencies=[1500],
                    temperature=523)
            0.09225237217593618
        """

        if freq_unit == 'cm-1':
            frequencies = [i * cm_to_eV for i in frequencies]
        elif freq_unit == 'eV':
            pass
        else:
            raise Exception("Unit for frequencies unknown.")
        thermo_ads = HarmonicThermo(
            vib_energies=frequencies,
            potentialenergy=potentialenergy)
        return thermo_ads.get_helmholtz_energy(temperature=temperature, verbose=False)

    # @staticmethod
    # def get_hindered_adsorbate_gibbs_free_energy(potentialenergy, temperature,
    #                                              trans_barrier_energy, rot_barrier_energy,
    #                                              rotationalminima, sitedensity, symmetrynumber,
    #                                              frequencies, freq_unit='cm-1', atoms=None):
    #     """
    #     Evaluate the Gibbs free energy of an adsorbed species with the
    #     hindered translator / hindered rotor model at a given temperature.

    #     :param potentialenergy: The potential energy in eV
    #     :type potentialenergy: float or int

    #     :param temperature: Temperature in K to calculate the Gibbs free energy at
    #     :type temperature: float or int

    #     :param trans_barrier_energy: Translational energy barrier in eV
    #     :type trans_barrier_energy: float or int

    #     :param rot_barrier_energy: Rotational energy barrier in eV
    #     :type rot_barrier_energy: float or int

    #     :param rotationalminima: Number of equivalent minima for an adsorbate's full rotation
    #     :type rotationalminima: float or int

    #     :param sitedensity: Density of surface sites in cm^-2
    #     :type sitedensity: float or int

    #     :param symmetrynumber: Symmetry number of the adsorbate
    #     :type symmetrynumber: int

    #     :param frequencies: The vibrational energies of the adsorbed species in cm-1 or eV.
    #         Note that frequencies should have a lower bound, e.g., 56 cm-1, in order to bound
    #         entropic contributions
    #     :type frequencies: list of floats or ints

    #     :param freq_unit: Vibrational energies unit
    #     :type freq_unit: str, 'cm-1' or 'eV', default: 'cm-1'

    #     :param atoms: ase.Atoms object
    #     :type atoms: ase.Atoms object, default: None

    #     :return: Gibbs free energy. Precisely speaking, the Helmholtz free energy is returned.
    #         However, the Gibbs free energy is the Helmholtz free energy plus a pV term,
    #         which is usually neglected as it is assumed to be small
    #     :rtype: float

    #     :Example:
    #         >>> TODO
    #         TODO
    #     """
    #     if not atoms:
    #         try:
    #             atoms = ase.build.molecule(name)
    #             logger.warn("Created an ase.Atoms object from the provided name "
    #                 "'{}' to calculate the moments of inertia and molecular mass."
    #                 "".format(name))
    #         except Exception as e:
    #             raise RuntimeError(e)
    #     if freq_unit == 'cm-1':
    #         frequencies = [i * cm_to_eV for i in frequencies]
    #     elif freq_unit == 'eV':
    #         pass
    #     else:
    #         raise Exception("Unknown unit of the provided frequencies.")
    #     thermo_ads = HinderedThermo(
    #         vib_energies=frequencies,
    #         trans_barrier_energy=trans_barrier_energy,
    #         rot_barrier_energy =rot_barrier_energy,
    #         sitedensity=sitedensity,
    #         rotationalminima= rotationalminima,
    #         potentialenergy=potentialenergy,
    #         atoms=atoms,
    #         symmetrynumber=symmetrynumber)
    #     return thermo_ads.get_helmholtz_energy(temperature=temperature, verbose=False)

# Frequencies are experimental data from NIST CCCBDB and are in cm-1
# Geometry should be 'monatomic', 'linear', or 'nonlinear'
# Symmetry numbers can be found in Table 10.1 and Appendix B of
# C. Cramer "Essentials of Computational Chemistry", 2nd Ed. and
# depend on the mode of adsorption; see ase.thermochemistry module

gas_species = {
    'H2': {
        # Harmonic Frequency(cm-1)
        'frequencies': [4401],
        'geometry': 'linear',
        'symmetrynumber': 2,
        'spin': 0,
    },
    'CH4': {
        # Fundmental Frequency(cm-1)
        'frequencies': [2917, 1534, 1534,
                        3019, 3019, 3019,
                        1306, 1306, 1306],
        'geometry': 'nonlinear',
        'symmetrynumber': 12,
        'spin': 0,
    },
    'O2': {
        # Harmonic Frequency(cm-1)
        'frequencies': [1580],
        'geometry': 'linear',
        'symmetrynumber': 2,
        'spin': 1,
    },
    'NO': {
        # Harmonic Frequency(cm-1)
        'frequencies': [1904],
        'geometry': 'linear',
        'symmetrynumber': None,
        'spin': 0.5,
    },
    'NO2': {
        # Harmonic Frequency(cm-1)
        'frequencies': [1358, 757, 1666],
        'geometry': 'nonlinear',
        'symmetrynumber': None,
        'spin': 0.5,
    },
    'NO3': {
        # Fundmental Frequency(cm-1)
        'frequencies': [1050, 762, 1492,
                        1492, 360, 360],
        'geometry': 'nonlinear',
        'symmetrynumber': None,
        'spin': 0.5,
    },
    'CO': {
        # Harmonic Frequency(cm-1)
        'frequencies': [2170],
        'geometry': 'linear',
        'symmetrynumber': 1,
        'spin': 0,
    },
    'CO2': {
        # Harmonic Frequency(cm-1)
        'frequencies': [1351, 2396, 672,
                        672],
        'geometry': 'linear',
        'symmetrynumber': None,
        'spin': 0,
    },
    'NH3': {
        # Fundmental Frequency(cm-1)
        'frequencies': [3337, 950, 3444,
                        3444, 1627, 1627],
        'geometry': 'nonlinear',
        'symmetrynumber': None,
        'spin': 0,
    },
    'C2H4': {
        # Fundmental Frequency(cm-1)
        'frequencies': [3026, 1623, 1342,
                        1023, 2989, 1444,
                        940, 3105, 826,
                        3086, 1217, 949],
        'geometry': 'nonlinear',
        'symmetrynumber': None,
        'spin': 0,
    },
    'HCl': {
        # Harmonic Frequency(cm-1)
        'frequencies': [2991],
        'geometry': 'linear',
        'symmetrynumber': None,
        'spin': 0,
    },
    'Cl2': {
        # Harmonic Frequency(cm-1)
        'frequencies': [560],
        'geometry': 'linear',
        'symmetrynumber': None,
        'spin': 0,
    },
    'H2O': {
        # Fundamental Frequency(cm-1)
        'frequencies': [3657, 1595, 3756],
        'geometry': 'nonlinear',
        'symmetrynumber': 2,
        'spin': 0,
    },
    'CH3CHO': {
        # Fundamental Frequency(cm-1)
        'frequencies': [3014, 2923, 2716,
                        1743, 1433, 1395,
                        1352, 1114, 867,
                        509, 2964, 1431,
                        1102, 764, 150],
        'geometry': 'nonlinear',
        'symmetrynumber': 1,
        'spin': 0,
    },
    'CH3CH2OH': {
        # Fundamental Frequency(cm-1)
        # In the old species definition the last
        # two frequencies were missing
        'frequencies': [3653, 2984, 2939,
                        2900, 1490, 1464,
                        1412, 1371, 1256,
                        1091, 1028, 888,
                        417, 2991, 2910,
                        1446, 1275, 1161,
                        812, 251, 200],
        'geometry': 'nonlinear',
        'symmetrynumber': 1,
        'spin': 0,
    },
}

adsorbate_species = {
    'H': {
        'frequencies': [],
        'symmetrynumber': None,
        'atoms': ase.Atoms('H'),
    },
    'CH4': {
        'frequencies': [],
        'symmetrynumber': None,
        'atoms': ase.Atoms('CH4',
                           [[-2.14262, 3.03116, 0.00000],
                            [-1.07262, 3.03116, 0.00000],
                            [-2.49979, 4.03979, 0.00000],
                            [-2.51306, 2.50700, 0.85611],
                            [-2.49435, 2.53348, -0.87948]],
                          ),
    },
    'O': {
        'frequencies': [],
        'symmetrynumber': None,
        'atoms': ase.Atoms('O'),
    },
    'CO': {
        'frequencies': [],
        'symmetrynumber': None,
        'atoms': ase.Atoms('CO',
                           [[0, 0, 0],
                            [0, 0, 1.2]],
                          ),
    },
    'NO': {
        'frequencies': [],
        'symmetrynumber': None,
        'atoms': ase.Atoms('NO',
                           [[0, 0, 0],
                            [0, 0, 1.2]],
                          ),
    },
}

class KMC_ViewBox(GUI):
    """
    The part of the viewer GUI that displays the model's
    current configuration.
    """

    def __init__(self, image_queue):
        super().__init__()
        self.image_queue = image_queue
        self.window_title = 'ASE view'
        self.window.win.iconphoto(False, tk.PhotoImage(
            file=os.path.join(APP_ABS_PATH, 'kmos3_logo.png')))
        self.adjust()
        self.update()
        self.run()

    def adjust(self):
        """
        Adjust the view of the model representation to
        nicely fit the viewer window.
        """
        self.window.win.iconphoto(False, tk.PhotoImage(
            file=os.path.join(APP_ABS_PATH, 'kmos3_logo.png')))
        atoms = self.image_queue.get()
        self.window.win.title(self.window_title)
        # will fail if no representation is provided
        try:
            covalent_radii = self.get_covalent_radii(atoms)
            _p = np.dot(atoms.get_positions(), self.axes)
            _p -= covalent_radii[:, None]
            _p1 = _p.min(0)
            _p += 2 * covalent_radii[:, None]
            _p2 = _p.max(0)
            self.center = np.dot(self.axes, (_p1 + _p2) / 2)
            # Add 30% of whitespace on each side of the atoms
            _s = 1.3 * (_p2 - _p1)
            width, height = self.window.size
            if _s[0] * height < _s[1] * width:
                self.scale = height / _s[1]
            elif _s[0] > 0.0001:
                self.scale = width / _s[0]
            else:
                self.scale = 1.0
        except:
            self.center = [0.0, 0.0, 0.0]
            self.scale = 1.0

    def get_menu_data(self):
        """
        Overriding function to not display the ASE menu.
        """
        return [['',
                 [M('', self.toggle_show_bonds, '', 0), M('', self.toggle_show_unit_cell, '', 0),
                  M('', self.toggle_show_velocities, '', 0), M('', self.toggle_show_forces, '', 0),
                  M('', self.show_labels, '', 0), M('', self.toggle_show_axes, '', 0)]]]

    def update(self):
        """Update the ViewBox."""
        if not self.image_queue.empty():
            atoms = self.image_queue.get()
            self.images.initialize([atoms], filenames=[self.window_title])
            self.set_frame()
        self.window.after(1, self.update)

class MyPNG(PNG):
    def __init__(self, atoms,
                 rotation='',
                 show_unit_cell=False,
                 radii=None,
                 bbox=None,
                 colors=None,
                 model=None,
                 scale=20) :

        self.atoms = atoms
        self.numbers = atoms.get_atomic_numbers()
        self.colors = colors
        self.model = model
        if colors is None:
            self.colors = ase.data.colors.jmol_colors[self.numbers]

        if radii is None:
            radii = ase.data.covalent_radii[self.numbers]
        elif type(radii) is float:
            radii = ase.data.covalent_radii[self.numbers] * radii
        else:
            radii = np.array(radii)

        natoms = len(atoms)

        if isinstance(rotation, str):
            rotation = ase.utils.rotate(rotation)

        A = atoms.get_cell()
        if show_unit_cell > 0:
            L, T, D = self.cell_to_lines(A)
            C = np.empty((2, 2, 2, 3))
            for c1 in range(2):
                for c2 in range(2):
                    for c3 in range(2):
                        C[c1, c2, c3] = np.dot([c1, c2, c3], A)
            C.shape = (8, 3)
            C = np.dot(C, rotation)  # Unit cell vertices
        else:
            L = np.empty((0, 3))
            T = None
            D = None
            C = None

        nlines = len(L)

        X = np.empty((natoms + nlines, 3))
        R = atoms.get_positions()
        X[:natoms] = R
        X[natoms:] = L

        r2 = radii**2
        for n in range(nlines):
            d = D[T[n]]
            if ((((R - L[n] - d)**2).sum(1) < r2) &
                (((R - L[n] + d)**2).sum(1) < r2)).any():
                T[n] = -1

        X = np.dot(X, rotation)
        R = X[:natoms]

        if bbox is None:
            X1 = (R - radii[:, None]).min(0)
            X2 = (R + radii[:, None]).max(0)
            if show_unit_cell == 2:
                X1 = np.minimum(X1, C.min(0))
                X2 = np.maximum(X2, C.max(0))
            M = (X1 + X2) / 2
            S = 1.05 * (X2 - X1)
            w = scale * S[0]
            #if w > 500:
                #w = 500
                #scale = w / S[0]
            h = scale * S[1]
            offset = np.array([scale * M[0] - w / 2, scale * M[1] - h / 2, 0])
        else:
            w = (bbox[2] - bbox[0]) * scale
            h = (bbox[3] - bbox[1]) * scale
            offset = np.array([bbox[0], bbox[1], 0]) * scale

        self.w = w
        self.h = h

        X *= scale
        X -= offset

        if nlines > 0:
            D = np.dot(D, rotation)[:, :2] * scale

        if C is not None:
            C *= scale
            C -= offset

        A = np.dot(A, rotation)
        A *= scale

        self.A = A
        self.X = X
        self.D = D
        self.T = T
        self.C = C
        self.natoms = natoms
        self.d = 2 * scale * radii

    def write(self, filename, resolution=72):
        self.filename = filename
        self.write_header(resolution=resolution)
        self.write_info()
        self.write_trailer(resolution=resolution)

    def write_info(self):
        def latex_float(f):
            float_str = "{0:.2e}".format(f)
            if "e" in float_str:
                base, exponent = float_str.split("e")
                return r"{0} \times 10^{{{1}}}".format(base, int(exponent))
            else:
                return float_str

        import matplotlib.text
        if self.model is not None:
            time = latex_float(self.model.base.get_kmc_time())

            text = matplotlib.text.Text(.05*self.w,
                                        .9*self.h,
                                        r'$t = {time}\,{{\rm s}}$'.format(**locals()),
                                        fontsize=36,
                                        bbox={'facecolor':'white', 'alpha':0.5, 'ec':'white', 'pad':1, 'lw':0 },
                                        )
            text.figure = self.figure
            text.draw(self.renderer)

    def write_header(self, resolution=72):
        from matplotlib.backends.backend_agg import RendererAgg
        from matplotlib.backend_bases import GraphicsContextBase
        from matplotlib.figure import Figure

        try:
            from matplotlib.transforms import Value
        except ImportError:
            dpi = resolution
        else:
            dpi = Value(resolution)

        self.renderer = RendererAgg(self.w, self.h, dpi)
        self.figure = Figure()

        self.gc = GraphicsContextBase()
        self.gc.set_linewidth(.2)

    def write_trailer(self, resolution=72):
        import matplotlib
        renderer = self.renderer
        if hasattr(renderer._renderer, 'write_png'):
            # Old version of matplotlib:
            renderer._renderer.write_png(self.filename)
        else:
            from ase.io import write
            #self.atoms.rotate(a=0.5, (0,1,0), rotate_self=True)
            if self.filename == "":
                write('atomic_view.png', self.atoms)
            else:
                if self.filename[-4:] == '.png':
                    self.filename.replace('.png', '.png')
                else:
                    self.filename = self.filename + '.png'
                write(self.filename, self.atoms)
