#!/usr/bin/env python3

"""
With kmos3 you can:

    * store and exchange KMC models through XML
    * generate fast, platform independent, self-contained code [#code]_
    * run KMC models through GUI or python bindings

Kmos3 has been developed in the context of first-principles based modelling
of surface chemical reactions but might be of help for other types of
KMC models as well.

The goal of kmos3 is to significantly reduce the time you need
to implement and run a lattice KMC simulation. However, it can not help
you plan the model.

Typical users will run kmos3 entirely from python code by following the examples.

.. rubric:: Footnotes

.. [#code] The source code is generated in Fortran90, written in a
    modular fashion. Python bindings are generated using
    `f2py  <https://numpy.org/doc/stable/f2py/>`_.
"""

#    Copyright 2009-2013 Max J. Hoffmann (mjhoffmann@gmail.com)
#    This file is part of kmos3.
#
#    kmos3 is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    kmos3 is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with kmos3.  If not, see <http://www.gnu.org/licenses/>.


import logging
import math
import tokenize

from kmos3 import cli
from kmos3 import species
from kmos3 import units


NEW_RATE_EVALUATION = True

logger = logging.getLogger(__name__)

__version__ = "3.1.0"
VERSION = __version__

rate_aliases = {'beta': '(1./(kboltzmann*T))'}

if NEW_RATE_EVALUATION:
    def evaluate_rate_expression(rate_expr, parameters=None):
        """
        Evaluates the mathematical expression for an input parameter.
        """

        import io # needs to be here to not import the kmos.io module
        # convert parameters to dict if passed as list of Parameters()
        if isinstance(parameters, list):
            param_dict = {}
            for parameter in parameters:
                param_dict[parameter.name] = {'value': parameter.value}
            parameters = param_dict
        if not rate_expr:
            rate_const = 0.0
        else:
            replaced_tokens = []
            for old, new in list(rate_aliases.items()):
                rate_expr = rate_expr.replace(old, new)
            try:
                input_str = io.StringIO(rate_expr).readline
                tokens = list(tokenize.generate_tokens(input_str))
            except:
                raise Exception('Could not tokenize expression: %s' % input_str)
            for i, token, _, _, _ in tokens:
                if token in ['sqrt', 'exp', 'sin', 'cos', 'pi', 'pow', 'log']:
                    replaced_tokens.append((i, 'math.' + token))
                # replace units used in parameters
                elif token in dir(units):
                    replaced_tokens.append((i, 'units.' + token))
                # recursively evaluate nested expressions
                elif token in parameters:
                    res = evaluate_rate_expression(str(parameters[token]['value']), parameters)
                    replaced_tokens.append((i, str(res)))
                elif token.startswith('m_'):
                    replaced_tokens.append((i, str(species.get_mass(token.split('_', 1)[1]))))
                elif token.startswith('mu_'):
                    # evaluate gas phase chemical potential if among
                    # available JANAF tables if from current temperature
                    # and corresponding partial pressure
                    species_name = token.split('_', 1)[1].rstrip('gas')
                    if species_name in species.gas_species:
                        if 'T' not in parameters:
                            raise Exception('Need "T" in parameters to evaluate chemical potential.')
                        if 'p_%s' % species_name in parameters:
                            p_name = species_name
                        elif 'p_%sgas' % species_name in parameters:
                            p_name = 'p_%sgas' % species_name
                        else:
                            raise Exception('Need "p_%s" in parameters to evaluate '
                                            'chemical potential.' % species_name)
                        replaced_tokens.append((
                            i, 'species.gas_species[\"%s\"].get_chemical_potential(%s,%s)' % (
                                species_name,
                                parameters['T']['value'],
                                parameters[p_name]['value'],
                                )))
                    else:
                        logger.info('No JANAF table assigned for %s'
                                    'Setting chemical potential to zero', species_name)
                        replaced_tokens.append((i, '0'))

                elif token.startswith('GibbsGas_'):
                    # evaluate gas phase gibbs free energy using ase thermochemistry module,
                    # experimental data from NIST CCCBDB, the electronic energy
                    # and current temperature and partial pressure
                    species_name = token.split('_', 1)[1]
                    if species_name.rstrip('gas') in species.gas_species:
                        if 'T' not in parameters:
                            raise Exception('Need "T" in parameters to evaluate gas phase '
                                            'gibbs free energy.')

                        if 'p_%s' % species_name not in parameters:
                            raise Exception('Need "p_%s" in parameters to evaluate gas phase '
                                            'gibbs free energy.' % species_name)
                        free_nrg = 'species.gas_species[\"%s\"].get_ideal_gas_gibbs_free_energy(%s,%s,%s)' % (
                            species_name.rstrip('gas'),
                            parameters['E_'+species_name]['value'],
                            parameters['T']['value'],
                            parameters['p_%s' % species_name]['value'],
                            )
                        replaced_tokens.append((i, str(free_nrg)))
                    else:
                        logger.info('No NIST data assigned for %s'
                                    'Setting chemical potential to zero', species_name)
                        replaced_tokens.append((i, '0'))
                elif token.startswith('GibbsAds_'):
                    # evaluate gibbs free energy of adsorbate using ase thermochemistry module,
                    # calculated frequencies and electronic energy and current temperature
                    species_name = token.split('_', 1)[1]
                    if 'T' not in parameters:
                        raise Exception('Need "T" in parameters to evaluate adsorbate '
                                        'gibbs free energy.')
                    energy = evaluate_rate_expression(str(parameters['E_'+species_name]['value']),
                                                        parameters)
                    free_nrg = "species.AdsorbateSpecies.get_harmonic_adsorbate_gibbs_free_energy("\
                            "potentialenergy={},frequencies={},temperature={})"\
                            "".format(energy,
                                        parameters['f_'+species_name]['value'],
                                        parameters['T']['value'])
                    replaced_tokens.append((i, str(free_nrg)))
                else:
                    replaced_tokens.append((i, token))

            rate_expr = tokenize.untokenize(replaced_tokens)
            try:
                rate_const = eval(rate_expr)
            except Exception as exc:
                raise UserWarning(
                    "Could not evaluate rate expression: %s\nException: %s"
                    % (rate_expr, exc))
        return rate_const

else:
    def evaluate_rate_expression(rate_expr, parameters={}):
        """Evaluates an expression for a typical kMC rate constant.
        External parameters can be passed in as dictionary, like the
        following:
            parameters = {'p_CO':{'value':1},
                        'T':{'value':1}}

        or as a list of parameters:
            parameters = [Parameter(), ... ]
        """

        import io # needs to be here to not import the kmos.io module
        test_copy = rate_expr
        # convert parameters to dict if passed as list of Parameters()
        if type(parameters) is list:
            param_dict = {}
            for parameter in parameters:
                param_dict[parameter.name] = {'value': parameter.value}
            parameters = param_dict

        if not rate_expr:
            rate_const = 0.0
        else:
            replaced_tokens = []

            # replace some aliases
            for old, new in list(rate_aliases.items()):
                rate_expr = rate_expr.replace(old, new)
            try:
                input = io.StringIO(rate_expr).readline
                tokens = list(tokenize.generate_tokens(input))
            except:
                raise Exception('Could not tokenize expression: %s' % input)
            for i, token, _, _, _ in tokens:
                if token in ['sqrt', 'exp', 'sin', 'cos', 'pi', 'pow', 'log']:
                    replaced_tokens.append((i, 'math.' + token))
                elif token in dir(units):
                    # print("TOKENo", token)
                    replaced_tokens.append((i, str(eval('units.' + token))))
                    # print("TOKENo", replaced_tokens)
                elif token.startswith('m_'):
                    from ase.symbols import string2symbols # TODO remove 'evaluate_rate_expression'
                    from ase.data import atomic_masses # TODO remove 'evaluate_rate_expression'
                    from ase.data import atomic_numbers # TODO remove 'evaluate_rate_expression'
                    species_name = '_'.join(token.split('_')[1:])
                    symbols = string2symbols(species_name)
                    replaced_tokens.append((
                        i, '%s' % sum([atomic_masses[atomic_numbers[symbol]] for symbol in symbols])))
                elif token.startswith('mu_'):
                    # evaluate gas phase chemical potential if among
                    # available JANAF tables if from current temperature
                    # and corresponding partial pressure
                    species_name = '_'.join(token.split('_')[1:])# + 'gas'
                    # print(dir(species))
                    if species_name in dir(species):
                        if not 'T' in parameters:
                            raise Exception('Need "T" in parameters to evaluate chemical potential.')

                        if not 'p_%s' % species_name in parameters:
                            raise Exception('Need "p_%s" in parameters to evaluate '
                                            'chemical potential.' % species_name)

                        replaced_tokens.append((i, 'species.%s.mu(%s,%s)' % (
                            species_name,
                            parameters['T']['value'],
                            parameters['p_%s' % species_name]['value'],
                            )))
                    else:
                        print('No JANAF table assigned for %s' % species_name)
                        print('Setting chemical potential to zero')
                        replaced_tokens.append((i, '0'))

                elif token.startswith('GibbsGas_'):
                    #evaluate gas phase gibbs free energy using ase thermochemistry module,
                    #experimental data from NIST CCCBDB, the electronic energy
                    #and current temperature and partial pressure
                    species_name = '_'.join(token.split('_')[1:])
                    if species_name in dir(species):
                        if not 'T' in parameters:
                            raise Exception('Need "T" in parameters to evaluate gas phase '
                                            'gibbs free energy.')

                        if not 'p_%s' % species_name in parameters:
                            raise Exception('Need "p_%s" in parameters to evaluate gas phase '
                                            'gibbs free energy.' % species_name)

                        # replaced_tokens.append((i, 'species.%s.GibbsGas(%s,%s,%s)' % (
                        replaced_tokens.append((i, 'species.%s.GibbsGas(%s,%s,%s)' % (
                            species_name,
                            parameters['E_'+species_name]['value'],
                            parameters['T']['value'],
                            parameters['p_%s' % species_name]['value'],
                            )))
                        # print("This suceeded!")
                    else:
                        print('No NIST data assigned for %s' % species_name)
                        print('Setting chemical potential to zero')
                        replaced_tokens.append((i, '0'))

                    # gibbs=eval(replaced_tokens[-1][-1])
                    # print(species_name+': %.3f'%gibbs)
                    # print(species.gas_species)

                elif token.startswith('GibbsAds_'):
                    #evaluate gibbs free energy of adsorbate using ase thermochemistry module,
                    #calculated frequencies and electronic energy and current temperature
                    species_name = '_'.join(token.split('_')[1:])
                    if not 'T' in parameters:
                        raise Exception('Need "T" in parameters to evaluate adsorbate '
                                        'gibbs free energy.')
                    energy = parameters['E_'+species_name]['value']
                    try:
                        eval(energy)
                    except:
                        try:
                            replaced_tokens2 = []
                            input = io.StringIO(energy).readline
                            tokens2 = list(tokenize.generate_tokens(input))
                        except:
                            raise Exception('Could not tokenize expression: %s' % input)
                        for j, token2, _, _, _ in tokens2:
                            if token2 in parameters:
                                parameter_str = str(parameters[token2]['value'])
                                try:
                                    eval(parameter_str)
                                    replaced_tokens2.append((j, parameter_str))
                                except:
                                    try:
                                        input = io.StringIO(parameter_str).readline
                                        tokens3 = list(tokenize.generate_tokens(input))
                                    except:
                                        raise Exception('Could not tokenize expression: %s' % input)
                                    for k, token3, _, _, _ in tokens3:
                                        if token3 in parameters:
                                            parameter_str = str(parameters[token3]['value'])
                                            replaced_tokens2.append((k, parameter_str))
                                        else:
                                            replaced_tokens2.append((k, token3))
                            else:
                                replaced_tokens2.append((j, token2))
                        energy = tokenize.untokenize(replaced_tokens2)
                    replaced_tokens.append((i, 'species.GibbsAds(%s,%s,%s)' % (
                        energy,
                        parameters['f_'+species_name]['value'],
                        parameters['T']['value'],
                        )))

                    #gibbs=eval(replaced_tokens[-1][-1])
                    #print species_name+': %.3f'%gibbs

                elif token in parameters:
                    parameter_str = str(parameters[token]['value'])
                    # replace some aliases
                    parameter_str = parameter_str.replace('beta', '(1./(kboltzmann*T))')
                    # replace units used in parameters
                    # for unit in units.keys:
                    for unit in dir(units):
                        parameter_str = parameter_str.replace(
                            unit, '%s' % eval('units.%s' % unit))
                    try:
                        # print("TOKENo", token)
                        eval(parameter_str)
                        replaced_tokens.append((i, parameter_str))
                        # print("TOKENo", replaced_tokens)
                    except:
                        try:
                            replaced_tokens2 = []
                            input = io.StringIO(parameter_str).readline
                            tokens2 = list(tokenize.generate_tokens(input))
                        except:
                            raise Exception('Could not tokenize expression: %s' % input)
                        for i, token2, _, _, _ in tokens2:
                            if token2 in ['sqrt', 'exp', 'sin', 'cos', 'pi', 'pow', 'log']:
                                replaced_tokens2.append((i, 'math.' + token2))
                            elif token2.startswith('GibbsGas_'):
                                #evaluate gas phase gibbs free energy using ase thermochemistry module,
                                #experimental data from NIST CCCBDB, the electronic energy
                                #and current temperature and partial pressure
                                species_name = '_'.join(token2.split('_')[1:])
                                if species_name in dir(species):
                                    if not 'T' in parameters:
                                        raise Exception('Need "T" in parameters to evaluate gas '
                                                        'phase gibbs free energy.')

                                    if not 'p_%s' % species_name in parameters:
                                        raise Exception('Need "p_%s" in parameters to evaluate gas '
                                                        'phase gibbs free energy.' % species_name)

                                    replaced_tokens2.append((i, 'species.%s.GibbsGas(%s,%s,%s)' % (
                                        species_name,
                                        parameters['E_'+species_name]['value'],
                                        parameters['T']['value'],
                                        parameters['p_%s' % species_name]['value'],
                                        )))
                                else:
                                    print('No NIST data assigned for %s' % species_name)
                                    print('Setting chemical potential to zero')
                                    replaced_tokens2.append((i, '0'))

                                #gibbs=eval(replaced_tokens2[-1][-1])
                                #print species_name+': %.3f'%gibbs

                            elif token2.startswith('GibbsAds_'):
                                #evaluate gibbs free energy of adsorbate using ase thermochemistry
                                #module, calculated frequencies and electronic energy and current
                                #temperature
                                species_name = '_'.join(token2.split('_')[1:])
                                if not 'T' in parameters:
                                    raise Exception('Need "T" in parameters to evaluate adsorbate '
                                                    'gibbs free energy.')
                                energy = parameters['E_'+species_name]['value']
                                try:
                                    eval(energy)
                                except:
                                    try:
                                        replaced_tokens3 = []
                                        input = io.StringIO(energy).readline
                                        tokens3 = list(tokenize.generate_tokens(input))
                                    except:
                                        raise Exception('Could not tokenize expression: %s' % input)
                                    for j, token3, _, _, _ in tokens3:
                                        if token3 in parameters:
                                            parameter_str = str(parameters[token3]['value'])
                                            try:
                                                eval(parameter_str)
                                                replaced_tokens3.append((j, parameter_str))
                                            except:
                                                try:
                                                    input = io.StringIO(parameter_str).readline
                                                    tokens4 = list(tokenize.generate_tokens(input))
                                                except:
                                                    raise Exception('Could not tokenize expression: '
                                                                    '%s' % input)
                                                for k, token4, _, _, _ in tokens4:
                                                    if token4 in parameters:
                                                        parameter_str = str(parameters[token4]['value'])
                                                        replaced_tokens3.append((k, parameter_str))
                                                    else:
                                                        replaced_tokens3.append((k, token4))
                                        else:
                                            replaced_tokens3.append((j, token3))
                                    energy = tokenize.untokenize(replaced_tokens3)
                                replaced_tokens2.append((i, 'species.GibbsAds(%s,%s,%s)' % (
                                    energy,
                                    parameters['f_'+species_name]['value'],
                                    parameters['T']['value'],
                                    )))

                                #gibbs=eval(replaced_tokens2[-1][-1])
                                #print species_name+': %.3f'%gibbs

                            elif token2 in parameters:
                                replaced_tokens2.append((i, str(parameters[token2]['value'])))
                            else:
                                replaced_tokens2.append((i, token2))
                        parameter_str = tokenize.untokenize(replaced_tokens2)

                        #print parameter_str
                        #print token+': %.7f'%eval(parameter_str)

                        replaced_tokens.append((i, parameter_str))
                else:
                    replaced_tokens.append((i, token))
            rate_expr = tokenize.untokenize(replaced_tokens)
            try:
                rate_const = eval(rate_expr)
            except Exception as e:
                raise UserWarning(
                    "Could not evaluate rate expression: %s\nException: %s"
                    % (rate_expr, e))
        # if rate_const:
        #     assert round(rate_const/evaluate_rate_expression_new(test_copy, parameters), 15) == 1.
        # else:
        #     assert rate_const == evaluate_rate_expression_new(test_copy, parameters)
        return rate_const

def compile(kmc_model):
    """Export and compile the model XML file."""
    # You can find the various compile_options in get_options() in kmos3/cli.py
    export(kmc_model.filename + ' -b ' + kmc_model.backend + ' ' + kmc_model.compile_options)

def create_kmc_model(model_name=None):
    """Pointer to create_kmc_model in types.py, which creates the kmc_model."""
    from kmos3.types import create_kmc_model
    return create_kmc_model(model_name)

# kmos3 can be invoked directly from the command line in one of the following ways::
#     kmos3 [-h] [--version] {benchmark,build,export,settings-export,import,rebuild,
#                             shell,run,view,xml} [options]
# defining small wrapper functions to call cli if person types kmos3.export("MyFirstModel.xml") etc.
# Probably should have made an "@" decorator, but that's okay.
def build(arguments):
    """Wrapper to build the model with the cli module."""
    cli.main('build' + ' ' + arguments)
def export(arguments):
    """Wrapper to export the model with the cli module."""
    cli.main('export' + ' ' + arguments)
def help(arguments):
    """Wrapper to display the argparse help with the cli module."""
    cli.main(arguments + ' -h')
def rebuild(arguments):
    """Wrapper to rebuild the model with the cli module."""
    cli.main('rebuild' + ' ' + arguments)
def run(arguments):
    """Wrapper to interactively run the model with the cli module."""
    cli.main('run'+' ' + arguments)
def settings_export(arguments):
    """Wrapper to exoprt the 'kmc_settings.py' file the model with the cli module."""
    cli.main('settings-export'+' ' + arguments)
def shell(arguments):
    """Wrapper to interactively run the model with the cli module."""
    cli.main('shell'+' ' + arguments)
def version(arguments):
    """Wrapper to display the program version with the cli module."""
    cli.main('--version')
#TODO: should change the view.py file to viewer, and change this to view.
def viewer(arguments):
    """Wrapper to interactively run and view the model with the cli module."""
    cli.main('view'+' ' + arguments)
def xml(arguments):
    """Wrapper to display the model XML representation with the cli module."""
    cli.main('xml'+' ' + arguments)

#the following line was commented out Nov 19th 2022, it should not be here
#and may have been here for testing.
#from kmos3.io import clear_model
