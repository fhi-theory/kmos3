#!/usr/bin/env python3
"""Several utility functions that do not seem to fit somewhere
   else.
"""
#    Copyright 2009-2013 Max J. Hoffmann (mjhoffmann@gmail.com)
#    This file is part of kmos3.
#
#    kmos3 is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    kmos3 is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with kmos3.  If not, see <http://www.gnu.org/licenses/>.


import re
import logging
from time import time
from io import StringIO
from kmos3.utils.ordered_dict import OrderedDict

logger = logging.getLogger(__name__)

ValidationError = Exception

FCODE = """module kind
implicit none
contains
subroutine real_kind(p, r, kind_value)
  integer, intent(in), optional :: p, r
  integer, intent(out) :: kind_value

  if(present(p).and.present(r)) then
    kind_value = selected_real_kind(p=p, r=r)
  else
    if (present(r)) then
      kind_value = selected_real_kind(r=r)
    else
      if (present(p)) then
        kind_value = selected_real_kind(p)
      endif
    endif
  endif
end subroutine real_kind

subroutine int_kind(p, r, kind_value)
  integer, intent(in), optional :: p, r
  integer, intent(out) :: kind_value

  if(present(p).and.present(r)) then
    kind_value = selected_int_kind(p)
  else
    if (present(r)) then
      kind_value = selected_int_kind(r=r)
    else
      if (present(p)) then
        kind_value = selected_int_kind(p)
      endif
    endif
  endif
end subroutine int_kind

end module kind
"""


class CorrectlyNamed:

    """Syntactic Sugar class for use with kiwi, that makes sure that the name
    field of the class has a name field, that always complys with the rules
    for variables.
    """

    def __init__(self):
        pass

    def on_name__validate(self, _, name):
        """Called by kiwi upon chaning a string
        """
        if ' ' in name:
            return ValidationError('No spaces allowed')
        elif name and not name[0].isalpha():
            return ValidationError('Need to start with a letter')


def write_py(fileobj, images, **kwargs):
    """Write a ASE atoms construction string for `images`
       into `fileobj`.
    """
    import numpy as np

    if isinstance(fileobj, str):
        fileobj = open(fileobj, 'w')

    scaled_positions = kwargs['scaled_positions'] \
        if 'scaled_positions' in kwargs else True
    fileobj.write('from ase import Atoms\n\n')
    fileobj.write('import numpy as np\n\n')

    if not isinstance(images, (list, tuple)):
        images = [images]
    fileobj.write('images = [\n')

    for image in images:
        if hasattr(image, 'get_chemical_formula'):
            chemical_formula = image.get_chemical_formula(mode='reduce')
        else:
            chemical_formula = image.get_name()
        cell_string = repr(image.cell)
        cell_string = cell_string.replace('cell', '')
        cell_string = cell_string.replace('Cell', '')
        fileobj.write("    Atoms(symbols='%s',\n"
                      "          pbc=np.%s,\n"
                      "          cell=np.array(      %s),\n" % (
                          chemical_formula,
                          repr(image.pbc),
                          cell_string))

        if not scaled_positions:
            fileobj.write("          positions=np.array(      %s),\n"
                          % repr(list(image.positions)))
        else:
            fileobj.write(
                "          scaled_positions=np.array(      %s),\n"
                % repr(list((np.around(image.get_scaled_positions(), decimals=7)).tolist())))
        fileobj.write('),\n')

    fileobj.write(']')


def get_ase_constructor(atoms):
    """Return the ASE constructor string for `atoms`."""
    if isinstance(atoms, str):
        #return atoms
        atoms = eval(atoms)
    if type(atoms) is type([]):
        atoms = atoms[0]
    f = StringIO()
    write_py(f, atoms)
    f.seek(0)
    lines = f.readlines()
    f.close()
    astr = ''
    for i, line in enumerate(lines):
        if i >= 5 and i < len(lines) - 1:
            astr += line
    # astr = astr[:-2]
    return astr.strip()


def product(*args, **kwds):
    """Take two lists and return iterator producing
    all combinations of tuples between elements
    of the two lists."""
    # product('ABCD', 'xy') --> Ax Ay Bx By Cx Cy Dx Dy
    # product(range(2), repeat=3) --> 000 001 010 011 100 101 110 111
    pools = [tuple(arg) for arg in args] * kwds.get('repeat', 1)
    result = [[]]
    for pool in pools:
        result = [x + [y] for x in result for y in pool]
    for prod in result:
        yield tuple(prod)


def split_sequence(seq, size):
    """Take a list and a number n and return list
       divided into n sublists of roughly equal size.
    """
    newseq = []
    splitsize = 1.0 / size * len(seq)
    for i in range(size):
        newseq.append(seq[int(round(i * splitsize)):
                          int(round((i + 1) * splitsize))])
    return newseq

def evaluate_kind_values(infile, outfile):
    """Go through a given file and dynamically
    replace all selected_int/real_kind calls
    with the dynamically evaluated fortran code
    using only code that the function itself
    contains.

    """
    import re
    import os
    import sys
    import shutil
    from numpy.distutils.fcompiler import get_default_fcompiler
    sys.path.append(os.path.abspath(os.curdir))

    with open(infile) as infh:
        intext = infh.read()
    if not ('selected_int_kind' in intext.lower()
            or 'selected_real_kind' in intext.lower()):
        shutil.copy(infile, outfile)
        return

    def import_selected_kind():
        """Tries to import the module which provides
        processor dependent kind values. If the module
        is not available it is compiled from a here-document
        and imported afterwards.

        Warning: creates both the source file and the
        compiled module in the current directory.

        """
        try:
            import f2py_selected_kind
        except:
            # quick'n'dirty workaround for windoze
            if os.name == 'nt':
                f = open('f2py_selected_kind.f90', 'w')
                f.write(FCODE)
                f.close()
                from copy import deepcopy
                # save for later
                true_argv = deepcopy(sys.argv)
                sys.argv = (('%s -c --fcompiler=gnu95 --compiler=mingw32'
                             ' -m f2py_selected_kind'
                             ' f2py_selected_kind.f90')
                            % sys.executable).split()
                from numpy import f2py as f2py2e
                f2py2e.main()

                sys.argv = true_argv
            else:
                # Previous compile call
                # from numpy.f2py import compile
                fcompiler = os.environ.get('F2PY_FCOMPILER', get_default_fcompiler())
                # compile(FCODE, source_fn='f2py_selected_kind.f90',
                #         modulename='f2py_selected_kind',
                #         extra_args='--fcompiler={}'.format(fcompiler))
                fname = 'f2py_selected_kind'
                with open(fname+'.f90', 'w') as f:
                    f.write(FCODE)
                from subprocess import call
                command = [sys.executable, '-m', 'numpy.f2py', '--fcompiler=' + fcompiler,
                           '-m', fname, '-c', fname+'.f90']
                with open('compile_output.txt', 'a') as f:
                    call(command, stdout=f, stderr=f, bufsize=0)
            try:
                import f2py_selected_kind
            except Exception as e:
                raise Exception('Could not create selected_kind module\n'
                                + '%s\n' % os.path.abspath(os.curdir)
                                + '%s\n' % os.listdir('.')
                                + '%s\n' % e)
        return f2py_selected_kind.kind

    def parse_args(args):
        """
            Parse the arguments for selected_(real/int)_kind
            to pass them on to the Fortran module.

        """
        in_args = [x.strip() for x in args.split(',')]
        args = []
        kwargs = {}

        for arg in in_args:
            if '=' in arg:
                symbol, value = arg.split('=')
                kwargs[symbol] = eval(value)
            else:
                args.append(eval(arg))

        return args, kwargs

    def int_kind(args):
        """Python wrapper around Fortran selected_int_kind
        function.
        """
        args, kwargs = parse_args(args)
        return import_selected_kind().int_kind(*args, **kwargs)

    def real_kind(args):
        """Python wrapper around Fortran selected_real_kind
        function.
        """
        args, kwargs = parse_args(args)
        return import_selected_kind().real_kind(*args, **kwargs)

    infile = open(infile)
    outfile = open(outfile, 'w')
    int_pattern = re.compile((r'(?P<before>.*)selected_int_kind'
                              '\\((?P<args>.*)\\)(?P<after>.*)'),
                             flags=re.IGNORECASE)
    real_pattern = re.compile((r'(?P<before>.*)selected_real_kind'
                               '\\((?P<args>.*)\\)(?P<after>.*)'),
                              flags=re.IGNORECASE)
    for line in infile:
        real_match = real_pattern.match(line)
        int_match = int_pattern.match(line)
        if int_match:
            match = int_match.groupdict()
            line = '%s%s%s\n' % (
                match['before'],
                int_kind(match['args']),
                match['after'],)
        elif real_match:
            match = real_match.groupdict()
            line = '%s%s%s\n' % (
                match['before'],
                real_kind(match['args']),
                match['after'],)
        outfile.write(line)
    infile.close()
    outfile.close()


def build(options):
    """Build binary with f2py binding from complete
    set of source file in the current directory.

    """

    from os.path import isfile
    import os
    import sys
    from glob import glob

    src_files = ['kind_values_f2py.f90', 'base.f90']

    if isfile('base_acf.f90'):
        src_files.append('base_acf.f90')
    src_files.append('lattice.f90')
    if isfile('proclist_constants.f90'):
        src_files.append('proclist_constants.f90')
    if isfile('proclist_pars.f90'):
        src_files.append('proclist_pars.f90')

    src_files.extend(glob('nli_*.f90'))
    # src_files.extend(glob('get_rate_*.f90'))
    src_files.extend(glob('run_proc_*.f90'))
    src_files.append('proclist.f90')
    if isfile('proclist_acf.f90'):
        src_files.append('proclist_acf.f90')

    extra_flags = {}
    print(options)
    if options.no_optimize:
        #-ffixed-line-length-none is not used as it seems to be not needed as of Nov 20th, 2022
        extra_flags['gfortran'] = ('-ffree-line-length-none -ffree-form'
                                   ' -xf95-cpp-input -Wall -fimplicit-none'
                                   ' -time  -fmax-identifier-length=63 ')
        extra_flags['gnu95'] = extra_flags['gfortran']
        extra_flags['intel'] = '-fpp -Wall -I/opt/intel/fc/10.1.018/lib'
        extra_flags['intelem'] = '-fpp -Wall'

    else:
        #-ffixed-line-length-none is not used as it seems to be not needed as of Nov 20th, 2022
        extra_flags['gfortran'] = ('-ffree-line-length-none -ffree-form'
                                   ' -xf95-cpp-input -Wall -O3 -fimplicit-none'
                                   ' -time -fmax-identifier-length=63 ')
        extra_flags['gnu95'] = extra_flags['gfortran']
        extra_flags['intel'] = '-fast -fpp -Wall -I/opt/intel/fc/10.1.018/lib'
        extra_flags['intelem'] = '-fast -fpp -Wall'

    # FIXME
    extra_libs = ''
    ccompiler = ''
    if os.name == 'nt':
        ccompiler = '--compiler=mingw64'#'--compiler=mingw32'
        if sys.version_info < (2, 7):
            extra_libs = ' -lmsvcr71 '
        else:
            extra_libs = ' -lmsvcr90 '

    module_name = 'kmc_model'

    if not isfile('kind_values_f2py.f90'):
        logger.info("Preparing and compling 'kind_values_f2py.f90'.")
        evaluate_kind_values('kind_values.f90', 'kind_values_f2py.f90')

    for src_file in src_files:
        if not isfile(src_file):
            raise IOError('File %s not found' % src_file)

    call = []
    call.append('-c')
    call.append('-c')
    call.append('--fcompiler=%s' % options.fcompiler)
    if os.name == 'nt':
        call.append('%s' % ccompiler)
    extra_flags = extra_flags.get(options.fcompiler, '')

    if options.debug:
        extra_flags += ' -DDEBUG'
    #NB presence of " around f90flags argument confuses f2py.
    #NB Command line argument separation already set by
    #NB split into separate str items in list. Not
    #NB sure why it ever worked.
    #NB call.append('--f90flags="%s"' % extra_flags)
    call.append('--f90flags=%s' % extra_flags)
    call.append('-m')
    call.append(module_name)
    call += src_files

    from copy import deepcopy
    true_argv = deepcopy(sys.argv)  # save for later
    from numpy import f2py
    sys.argv = call
    logger.info("Starting to compile the model.\n...")
    try:
        # f2py.main compiler call will not let one capture the stdout/stderr
        from subprocess import call
        command = [sys.executable, '-m', 'numpy.f2py', '--fcompiler=' + options.fcompiler,
                   '--f90flags=' + extra_flags, '-m', module_name, '-c'] + src_files
        print(command)
        with open('compile_output.txt', 'w') as f:
            call(command, stdout=f, stderr=f, bufsize=0)
    except:
        f2py.main() # Doesn't work according to Alberdi, but works in Erwin's.
    else:
        with open('compile_output.txt', 'r') as f:
            # looking for 'exit status 1'
            f.seek(0, 2)
            f.seek(f.tell()-14, 0)
            last = f.read()[:-1]
            if last == 'exit status 1':
                raise RuntimeError(last)
            else:
                logger.debug("Compilation ends with: %s", last)
        logger.info("Done compiling.")
    sys.argv = true_argv


def T_grid(T_min, T_max, n):
    from numpy import linspace, array
    """Return a list of n temperatures between
       T_min and T_max such that the grid of T^(-1)
       is evenly spaced.
    """

    T_min1 = T_min ** (-1.)
    T_max1 = T_max ** (-1.)

    grid = list(linspace(T_max1, T_min1, n))
    grid.reverse()
    grid = [x ** (-1.) for x in grid]

    return array(grid)


def p_grid(p_min, p_max, n):
    from numpy import logspace, log10
    """Return a list of n pressures between
       p_min and p_max such that the grid of log(p)
       is evenly spaced.
    """
    p_minlog = log10(p_min)
    p_maxlog = log10(p_max)

    grid = logspace(p_minlog, p_maxlog, n)

    return grid


def timeit(func):
    """
    Generic timing decorator

    To stop time for function call f
    just ::

        from kmos3.utils import timeit
        @timeit
        def f():
            ...

     """
    def wrapper(*args, **kwargs):
        time0 = time()
        func(*args, **kwargs)
        print('Executing %s took %.3f s' % (func.__name__, time() - time0))
    return wrapper


def col_tuple2str(tup):
    """Convenience function that turns a HTML type color
    into a tuple of three float between 0 and 1
    """
    r, g, b = tup
    b *= 255
    res = '#'
    res += hex(int(255 * r))[-2:].replace('x', '0')
    res += hex(int(255 * g))[-2:].replace('x', '0')
    res += hex(int(255 * b))[-2:].replace('x', '0')

    return res


def col_str2tuple(hex_string):
    """Convenience function that turns a HTML type color
    into a tuple of three float between 0 and 1
    """
    if isinstance(hex_string, str) and len(hex_string) in [7, 9]:
        return tuple(int(hex_string[i:i+2], 16)/255. for i in (1, 3, 5))
    else:
        raise UserWarning('Cannot decipher color string {}'.format(hex_string))


def jmolcolor_in_hex(i):
    """Return a given jmol color in hexadecimal representation."""
    try:
        color = jmol_colors[i]
        r, g, b = color
        a = 255
        color = (r << 24) | (g << 16) | (b << 8) | a
    except:
        color = 0
    return color


def evaluate_template(template, escape_python=False, **kwargs):
    """Very simple template evaluation function using only exec and str.format()

    There are two flavors of the template language, depending on whether
    the python parts or the template parts are escaped.

    A template can use the full python syntax. Every line starts with '#@ '
    is interpreted as a template line. Please use proper indentation before
    and note the space after '#@'.

    The template lines are converted to TEMPLATE_LINE.format(locals())
    and thefore every variable in the template line should be escape
    with {}.

    A valid template could be

    for i in range:
        #@ Hello World {i}

    """
    locals().update(kwargs)

    result = ''
    NEWLINE = '\n'
    PREFIX = '#@'
    lines = [line + NEWLINE for line in template.split(NEWLINE)]

    if escape_python:
        # first just replace verbose lines by pass to check syntax
        python_lines = ''
        matched = False
        for line in lines:
            if re.match('^\\s*%s ?' % PREFIX, line):
                python_lines += line.lstrip()[3:]
                matched = True
            else:
                python_lines += 'pass # %s' % line.lstrip()
        # if the tempate didn't contain any meta strings
        # just return the original
        if not matched:
            return template
        #NB python3 exec doesn't modify local variables.
        #NB create local dict and copy back "result" explicitly
        #NB if any other local variables are modified, that change
        #NB will be lost.
        ldict = locals().copy()
        exec(python_lines, globals(), ldict)
        result = ldict['result']

        # second turn literary lines into write statements
        python_lines = ''
        for line in lines:
            if re.match('^\\s*%s ' % PREFIX, line):
                python_lines += line.lstrip()[3:]
            elif re.match('^\\s*%s$' % PREFIX, line):
                python_lines += '%sresult += "\\n"\n' % (
                    ' ' * (len(line) - len(line.lstrip())))
            elif re.match('^$', line):
                # python_lines += 'result += """\n"""\n'
                pass
            else:
                python_lines += '%sresult += ("""%s""".format(**dict(locals())))\n' \
                    % (' ' * (len(line.expandtabs(4)) - len(line.lstrip())), line.lstrip())

        #NB see note above
        ldict = locals().copy()
        exec(python_lines, globals(), ldict)
        result = ldict['result']

    else:
        # first just replace verbose lines by pass to check syntax
        python_lines = ''
        matched = False
        for line in lines:
            if re.match('\\s*%s ?' % PREFIX, line):
                python_lines += '%spass %s' \
                    % (' ' * (len(line) - len(line.lstrip())),
                       line.lstrip())

                matched = True
            else:
                python_lines += line
        if not matched:
            return template
        #NB see note above
        ldict = locals().copy()
        exec(python_lines, globals(), ldict)
        result = ldict['result']

        # second turn literary lines into write statements
        python_lines = ''

        for line in lines:
            if re.match('\\s*%s ' % PREFIX, line):
                python_lines += '%sresult += ("""%s""".format(**dict(locals())))\n' \
                    % (' ' * (len(line) - len(line.lstrip())),
                       line.lstrip()[3:])
            elif re.match('\\s*%s' % PREFIX, line):
                python_lines += '%sresult += "\\n"\n' % (
                    ' ' * (len(line) - len(line.lstrip())))
            else:
                python_lines += line

        #NB see note above
        ldict = locals().copy()
        exec(python_lines, globals(), ldict)
        result = ldict['result']

    return result

# Taken from https://jmol.sourceforge.net/jscolors/
jmol_colors = [
    (255, 255, 255), # 1   H
    (217, 255, 255), # 2   He
    (204, 128, 255), # 3   Li
    (194, 255, 0),   # 4   Be
    (255, 181, 181), # 5   B
    (144, 144, 144), # 6   C
    (48, 80, 248),   # 7   N
    (255, 13, 13),   # 8   O
    (144, 224, 80),  # 9   F
    (179, 227, 245), # 10  Ne
    (171, 92, 242),  # 11  Na
    (138, 255, 0),   # 12  Mg
    (191, 166, 166), # 13  Al
    (240, 200, 160), # 14  Si
    (255, 128, 0),   # 15  P
    (255, 255, 48),  # 16  S
    (31, 240, 31),   # 17  Cl
    (128, 209, 227), # 18  Ar
    (143, 64, 212),  # 19  K
    (61, 255, 0),    # 20  Ca
    (230, 230, 230), # 21  Sc
    (191, 194, 199), # 22  Ti
    (166, 166, 171), # 23  V
    (138, 153, 199), # 24  Cr
    (156, 122, 199), # 25  Mn
    (224, 102, 51),  # 26  Fe
    (240, 144, 160), # 27  Co
    (80, 208, 80),   # 28  Ni
    (200, 128, 51),  # 29  Cu
    (125, 128, 176), # 30  Zn
    (194, 143, 143), # 31  Ga
    (102, 143, 143), # 32  Ge
    (189, 128, 227), # 33  As
    (255, 161, 0),   # 34  Se
    (166, 41, 41),   # 35  Br
    (92, 184, 209),  # 36  Kr
    (112, 46, 176),  # 37  Rb
    (0, 255, 0),     # 38  Sr
    (148, 255, 255), # 39  Y
    (148, 224, 224), # 40  Zr
    (115, 194, 201), # 41  Nb
    (84, 181, 181),  # 42  Mo
    (59, 158, 158),  # 43  Tc
    (36, 143, 143),  # 44  Ru
    (10, 125, 140),  # 45  Rh
    (0, 105, 133),   # 46  Pd
    (192, 192, 192), # 47  Ag
    (255, 217, 143), # 48  Cd
    (166, 117, 115), # 49  In
    (102, 128, 128), # 50  Sn
    (158, 99, 181),  # 51  Sb
    (212, 122, 0),   # 52  Te
    (148, 0, 148),   # 53  I
    (66, 158, 176),  # 54  Xe
    (87, 23, 143),   # 55  Cs
    (0, 201, 0),     # 56  Ba
    (112, 212, 255), # 57  La
    (255, 255, 199), # 58  Ce
    (217, 255, 199), # 59  Pr
    (199, 255, 199), # 60  Nd
    (163, 255, 199), # 61  Pm
    (143, 255, 199), # 62  Sm
    (97, 255, 199),  # 63  Eu
    (69, 255, 199),  # 64  Gd
    (48, 255, 199),  # 65  Tb
    (31, 255, 199),  # 66  Dy
    (0, 255, 156),   # 67  Ho
    (0, 230, 117),   # 68  Er
    (0, 212, 82),    # 69  Tm
    (0, 191, 56),    # 70  Yb
    (0, 171, 36),    # 71  Lu
    (77, 194, 255),  # 72  Hf
    (77, 166, 255),  # 73  Ta
    (33, 148, 214),  # 74  W
    (38, 125, 171),  # 75  Re
    (38, 102, 150),  # 76  Os
    (23, 84, 135),   # 77  Ir
    (208, 208, 224), # 78  Pt
    (255, 209, 35),  # 79  Au
    (184, 184, 208), # 80  Hg
    (166, 84, 77),   # 81  Tl
    (87, 89, 97),    # 82  Pb
    (158, 79, 181),  # 83  Bi
    (171, 92, 0),    # 84  Po
    (117, 79, 69),   # 85  At
    (66, 130, 150),  # 86  Rn
    (66, 0, 102),    # 87  Fr
    (0, 125, 0),     # 88  Ra
    (112, 171, 250), # 89  Ac
    (0, 186, 255),   # 90  Th
    (0, 161, 255),   # 91  Pa
    (0, 143, 255),   # 92  U
    (0, 128, 255),   # 93  Np
    (0, 107, 255),   # 94  Pu
    (84, 92, 242),   # 95  Am
    (120, 92, 227),  # 96  Cm
    (138, 79, 227),  # 97  Bk
    (161, 54, 212),  # 98  Cf
    (179, 31, 212),  # 99  Es
    (179, 31, 186),  # 100 Fm
    (179, 13, 166),  # 101 Md
    (189, 13, 135),  # 102 No
    (199, 0, 102),   # 103 Lr
    (204, 0, 89),    # 104 Rf
    (209, 0, 79),    # 105 Db
    (217, 0, 69),    # 106 Sg
    (224, 0, 56),    # 107 Bh
    (230, 0, 46),    # 108 Hs
    (235, 0, 38),    # 109 Mt
    ]
