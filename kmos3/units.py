#!/usr/bin/env python3

"""
    Several commonly used constants and conversion factors are offered
    by this module, such a pi, the speed of light, or the Planck constant.
    Source: CODATA2010
"""
#    Copyright 2009-2013 Max J. Hoffmann (mjhoffmann@gmail.com)
#    This file is part of kmos3.
#
#    kmos3 is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    kmos3 is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with kmos3.  If not, see <http://www.gnu.org/licenses/>.

keys = ['pi', 'c', 'h', 'hbar', 'eV', 'kboltzmann', 'umass', 'angstrom',
        'bar', 'cm_to_eV', 'kboltzmann_in_eVK', 'Jmol_in_eV']

pi = 3.14159265358979323846  # approximately ...
c = 2.99792458e8  # m / s
h = 6.62606957e-34  # J s
hbar = 1.054571726e-34  # J s
eV = 1.602176565e-19  # C
kboltzmann = 1.3806488e-23  # J K
umass = 1.660538921e-27  # kg atomic mass
angstrom = 1.e-10  # m
bar = 1.e5  # kg / m s^2
cm_to_eV = 1.23981e-4 # eV
kboltzmann_in_eVK = 8.6173324e-5  # eV / K
Jmol_in_eV = 1.03642e-5  # eV
