#!/usr/bin/env python3

"""
    Module to configure the kmos3 installation.
    It will determine whether packages required for
    addon modules are installed.
"""

#    Copyright 2009-2013 Max J. Hoffmann (mjhoffmann@gmail.com)
#    This file is part of kmos3.
#
#    kmos3 is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    kmos3 is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with kmos3.  If not, see <http://www.gnu.org/licenses/>.

import os
import logging


APP_ABS_PATH = os.path.dirname(os.path.abspath(__file__))
EXTENSIONS = {
    'ase': False,
    'janaf': False,
}
logging.basicConfig(format="%(levelname)-8s %(name)-20s %(message)s", level=logging.WARN)
# Switch off some loggers from other modules
if logging.root.level == logging.DEBUG:
    logging.getLogger('matplotlib.font_manager').disabled = True
    logging.getLogger('matplotlib').disabled = True
    logging.getLogger('matplotlib.ticker').disabled = True
    logging.getLogger('matplotlib.pyplot').disabled = True
    logging.getLogger('asyncio').disabled = True
    logging.getLogger('parso.cache').disabled = True
    logging.getLogger('parso.python.diff').disabled = True
logger = logging.getLogger(__name__)
try:
    from kmos3.extensions import ase_extension
    EXTENSIONS['ase'] = True
except ImportError:
    logger.warning("The Atomic Simulation Environment (ase) is not installed.")
try:
    from kmos3.extensions import janaf_extension
    EXTENSIONS['janaf'] = True
except ImportError:
    logger.warning("The JANAF extension is not installed.")
