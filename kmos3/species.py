#!/usr/bin/env python3

"""
Module that implements the base classes and functions for the calculation
of thermochemical corrections depending on the available extensions.
"""

#    Copyright 2009-2013 Max J. Hoffmann (mjhoffmann@gmail.com)
#    This file is part of kmos3.

#    kmos3 is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    kmos3 is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with kmos3.  If not, see <http://www.gnu.org/licenses/>.

import logging
import collections.abc
from kmos3.config import EXTENSIONS

logger = logging.getLogger(__name__)

def dict_merge(dict1, dict2):
    """
    Merge two dictionaries recursively.
    For keys present in both dictionaries, the ones
    from dictionary d1 will be overwritten.
    """

    for key, value in dict2.items():
        if isinstance(value, collections.abc.Mapping):
            dict1[key] = dict_merge(dict1.get(key, {}), value)
        else:
            dict1[key] = value
    return dict1

class AdsorbateSpeciesBase():
    """
    Stub class to for an adsorbate species.
    """

    def __init__(self, name='', **kwargs):
        self.name = name

    def __repr__(self):
        return self.name

    @staticmethod
    def get_harmonic_adsorbate_gibbs_free_energy(**kwargs):
        """
        Dummy function to return either the potential energy
        or 0 for the free energy of an adsorbate species.
        """

        if 'potentialenergy' in kwargs:
            logger.warning(
                "Atomic Simulation Environment (ase) not installed. "
                "Using the provided potential energy of %s.",
                kwargs['potentialenergy'])
            return kwargs['potentialenergy']
        logger.warning(
            "Atomic Simulation Environment (ase) not installed "
            "and no potential energy provided. Setting the Gibbs free energy "
            "to zero.")
        return 0.0

    @staticmethod
    def get_hindered_adsorbate_gibbs_free_energy(**kwargs):
        """
        Dummy function to return either the potential energy
        or 0 for the free energy of an adsorbate species.
        """

        if 'potentialenergy' in kwargs:
            logger.warning(
                "Atomic Simulation Environment (ase) not installed. "
                "Using the provided potential energy of %s.",
                kwargs['potentialenergy'])
            return kwargs['potentialenergy']
        logger.warning(
            "Atomic Simulation Environment (ase) not installed "
            "and no potential energy provided. Setting the Gibbs free energy "
            "to zero.")
        return 0.0

class GasSpeciesBase():
    """
    Stub class to for a gas species.
    """

    def __init__(self, name='', **kwargs):
        # super().__init__(**kwargs)
        self.name = name

    def __repr__(self):
        return self.name

    def get_chemical_potential(self, **kwargs):
        """
        Dummy function to return 0 for the chemical potential of a gas species.
        """

        logger.warning("No JANAF table for %s. Using a chemical potential of zero.", self.name)
        return 0.0

    def get_ideal_gas_gibbs_free_energy(self, **kwargs):
        """
        Dummy function to return either the potential energy
        or 0 for the free energy of a gas species.
        """

        if 'potentialenergy' in kwargs:
            logger.warning(
                "Atomic Simulation Environment (ase) not installed. "
                "Using the provided potential energy of %s for %s.",
                kwargs['potentialenergy'], self.name)
            return kwargs['potentialenergy']
        logger.warning(
            "Atomic Simulation Environment (ase) not installed "
            "and no potential energy provided. Setting the Gibbs free energy "
            "to zero.")
        return 0.0

class LiquidSpeciesBase():
    """
    Stub class to for a liquid species.
    """

    def __init__(self, name='', **kwargs):
        self.name = name

    def __repr__(self):
        return self.name

    def get_chemical_potential(self, **kwargs):
        """
        Dummy function to return 0 for the chemical potential of a liquid species.
        """

        logger.warning("No JANAF table for %s. Using a chemical potential of zero.", self.name)
        return 0.0

def get_mass(name):
    """
    Dummy function.
    """

    raise Exception("Atomic Simulation Environment (ase) required "
                    "to calculate the mass of {}.".format(name))

gas_species = {}
liquid_species = {}
adsorbate_species = {}

# Create classes dynamically depending on the available extensions.
inherit_gas = [GasSpeciesBase]
inherit_liquid = [LiquidSpeciesBase]
inherit_adsorbate = [AdsorbateSpeciesBase]

if EXTENSIONS['janaf']:
    from kmos3.extensions.janaf_extension import GasSpecies as JanafGasSpecies
    from kmos3.extensions.janaf_extension import gas_species as janaf_gas_species
    from kmos3.extensions.janaf_extension import liquid_species as janaf_liquid_species
    inherit_gas.insert(0, JanafGasSpecies)
    # gas_species.update(janaf_gas_species)
    # liquid_species.update(janaf_liquid_species)
    gas_species = dict_merge(gas_species, janaf_gas_species)
    liquid_species = dict_merge(liquid_species, janaf_liquid_species)

if EXTENSIONS['ase']:
    from kmos3.extensions.ase_extension import GasSpecies as AseGasSpecies
    from kmos3.extensions.ase_extension import AdsorbateSpecies as AseAdsorbateSpecies
    from kmos3.extensions.ase_extension import gas_species as ase_gas_species
    from kmos3.extensions.ase_extension import adsorbate_species as ase_adsorbate_species
    from kmos3.extensions.ase_extension import get_mass
    inherit_gas.insert(0, AseGasSpecies)
    inherit_adsorbate.insert(0, AseAdsorbateSpecies)
    # gas_species.update(ase_gas_species)
    # adsorbate_species.update(ase_adsorbate_species)
    gas_species = dict_merge(gas_species, ase_gas_species)
    adsorbate_species = dict_merge(adsorbate_species, ase_adsorbate_species)

class GasSpecies(*inherit_gas):
    """
    Class to hold the gas species properties for the
    estimation of its free energy.
    """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

class AdsorbateSpecies(*inherit_adsorbate):
    """
    Class to estimate the adsorbte species free energy.
    """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

class LiquidSpecies(*inherit_liquid):
    """
    Class to hold the liquid species properties for the
    estimation of its free energy.
    """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

def prepare_species(species_dict, species_class):
    """
    Prepare the classes with the respective species defined within kmos3.
    """

    result = {}
    for species, entries in species_dict.items():
        result[species] = species_class(name=species, **entries)
    return result

gas_species = prepare_species(gas_species, GasSpecies)
liquid_species = prepare_species(liquid_species, LiquidSpecies)
adsorbate_species = prepare_species(adsorbate_species, AdsorbateSpecies)

# Here for testing purposes of the old evaluate_rate_expression function

if EXTENSIONS['ase']:

    def GibbsAds(energy, frequencies, T):
        #Expecting T in Kelvin
        #note that frequencies should have a lower bound, e.g. 56 cm-1, in order
        #to bound entropic contributions.
        from ase.thermochemistry import HarmonicThermo
        cm_to_eV = 1.23981e-4
        vib_energies = list(frequencies)
        for i in range(len(vib_energies)):
            vib_energies[i] = vib_energies[i] * cm_to_eV
        thermo_ads = HarmonicThermo(vib_energies=vib_energies, potentialenergy=energy)
        #In older versions of ASE the Helmholtz energy was called the Gibbs free energy.
        #However, nothing has changed in what is actually calculated.
        #The two different free energies are approximately equal since their
        #difference (the pV term) is small.
        #This pV term was always neglected.
        val = thermo_ads.get_helmholtz_energy(temperature=T, verbose=False)
        return val

    import numpy as np
    import os
    from ase.build import molecule
    from ase import Atoms
    from ase.thermochemistry import IdealGasThermo

    class Species(object):
        def __init__(self, atoms, gas=False, janaf_file='', name='', symmetrynumber=0,
                    frequencies=[], geometry='', spin=0):
            self.atoms = atoms
            self.gas = gas
            if name:
                self.name = name
            else:
                if hasattr(self.atoms, 'get_chemical_formula'):
                    self.name = self.atoms.get_chemical_formula(mode='hill')
                else:
                    self.atoms.get_name()
            self.janaf_file = janaf_file
            self.symmetrynumber = symmetrynumber
            self.frequencies = frequencies
            self.geometry = geometry
            self.spin = spin

            # prepare chemical potential
            if self.gas and self.janaf_file:
                self._prepare_G_p0(
                    os.path.abspath(
                        self.janaf_file))

        def __repr__(self):
            return self.name

        def mu(self, T, p):
            """Expecting T in Kelvin, p in bar"""
            if self.gas:
                kboltzmann_in_eVK = 8.6173324e-5
                # interpolate given grid
                try:
                    val = np.interp(T, self.T_grid, self.G_grid) + \
                        kboltzmann_in_eVK * T * np.log(p)
                except Exception:
                    raise Exception('Could not find JANAF tables for %s.'
                                    % self.name)
                else:
                    return val

            else:
                raise UserWarning('%s is no gas-phase species.' % self.name)

        def GibbsGas(self, energy, T, p):
            #Expecting T in Kelvin, p in bar
            #note that frequencies should have a lower bound, e.g. 56 cm-1, in order to bound
            #entropic contributions.
            if self.gas:
                cm_to_eV = 1.23981e-4
                vib_energies = list(self.frequencies)
                for i in range(len(vib_energies)):
                    vib_energies[i] = vib_energies[i] * cm_to_eV
                thermo_gas = IdealGasThermo(
                    vib_energies=vib_energies,
                    potentialenergy=energy,
                    atoms=self.atoms,
                    geometry=self.geometry,
                    symmetrynumber=self.symmetrynumber,
                    spin=self.spin)
                bar_to_Pa = 1e5
                pressure = p * bar_to_Pa
                val = thermo_gas.get_gibbs_energy(temperature=T, pressure=pressure, verbose=False)
                return val

            else:
                raise UserWarning('%s is no gas-phase species.' % self.name)

        def _prepare_G_p0(self, filename):
            # from CODATA 2010
            Jmol_in_eV = 1.03642E-5
            # load data
            try:
                data = np.loadtxt(filename, skiprows=2, usecols=(0, 2, 4))
            except IOError:
                print('Warning: JANAF table %s not installed' % filename)
                return

            # define data
            self.T_grid = data[:, 0]
            self.G_grid = (1000 * (data[:, 2] - data[0, 2])
                        - data[:, 0] * data[:, 1]) * Jmol_in_eV

        def __eq__(self, other):
            return self.atoms == other.atoms and self.gas == other.gas

        def __ne__(self, other):
            return not self.__eq__(other)

        def __hash__(self):
            return id(self)


    # prepare all required species

    #frequencies are experimental data from NIST CCCBDB and are in cm-1
    #geometry should be 'monatomic', 'linear', or 'nonlinear'
    #symmetry numbers can be found in Table 10.1 and Appendix B of C. Cramer "Essentials of Computational Chemistry", 2nd Ed.
    #see ase.thermochemistry module

    H2gas = Species(
        molecule('H2'),
        gas=True,
        janaf_file=os.path.join(os.path.dirname(__file__), 'extensions/janaf_data/H-050.txt'),
        name='H2gas',
        frequencies=[4401],
        geometry='linear',
        symmetrynumber=2,
        spin=0
        )

    # H = Species(
    #         Atoms('H')
    #         )

    CH4gas = Species(
        molecule('CH4'),
        gas=True,
        janaf_file=os.path.join(os.path.dirname(__file__), 'extensions/janaf_data/C-067.txt'),
        name='CH4gas',
        frequencies=[2917, 1534, 1534, 3019, 3019, 3019, 1306, 1306, 1306],
        geometry='nonlinear',
        symmetrynumber=12,
        spin=0
        )

    # CH4 = Species(
    #         Atoms('CH4',
    #             [[-2.14262, 3.03116, 0.00000],
    #              [-1.07262, 3.03116, 0.00000],
    #              [-2.49979, 4.03979, 0.00000],
    #              [-2.51306, 2.50700, 0.85611],
    #              [-2.49435, 2.53348, -0.87948]],
    #         ),
    #         name='CH4'
    #         )

    # O = Species(
    #         Atoms('O',
    #             [[0, 0, 0]],
    #             cell=[10, 10, 10],
    #         ),
    #         name='O'
    #         )

    O2gas = Species(
            molecule('O2'),
            gas=True,
            janaf_file=os.path.join(os.path.dirname(__file__), 'extensions/janaf_data/O-029.txt'),
            name='O2gas',
            frequencies=[1580],
            geometry='linear',
            symmetrynumber=2,
            spin=1
            )

    # NOgas = Species(
    #         Atoms('NO',
    #             [[0, 0, 0],
    #              [0, 0, 1.2]],
    #             cell=[10, 10, 10],
    #         ),
    #         gas=True,
    #         janaf_file='N-005.txt',
    #         name='NOgas',
    #     )

    # NO = Species(
    #         Atoms('NO',
    #             [[0, 0, 0],
    #              [0, 0, 1.2]],
    #             cell=[10, 10, 10],
    #         ),
    #         name='NO',
    #         janaf_file='N-005.txt',
    #         )

    # NO2gas = Species(
    #         Atoms(),
    #         gas=True,
    #         janaf_file='N-007.txt',
    #         name='NO2gas'
    #         )

    # NO3gas = Species(
    #         Atoms(),
    #         gas=True,
    #         janaf_file='N-009.txt',
    #         name='NO3gas'
    #         )

    # COgas = None
    COgas = Species(
            molecule('CO'),
            gas=True,
            janaf_file=os.path.join(os.path.dirname(__file__), 'extensions/janaf_data/C-093.txt'),
            name='COgas',
            frequencies=[2170],
            geometry='linear',
            symmetrynumber=1,
            spin=0
            )

    # CO = Species(
    #         Atoms('CO',
    #               [[0, 0, 0],
    #                [0, 0, 1.2]],
    #               cell=[10, 10, 10],
    #             ),
    #         name='CO'
    #         )

    CO2gas = Species(Atoms('CO2',
        [[0, 0, -1.2],
        [0, 0, 0],
        [0, 0, 1.2]],
        cell=[10, 10, 10],
        ),
        gas=True,
        janaf_file=os.path.join(os.path.dirname(__file__), 'extensions/janaf_data/C-095.txt'),
        name='CO2gas')

    # NH3gas = Species(Atoms(symbols='NH3',
    #           pbc=np.array([True,  True,  True], dtype=bool),
    #           cell=np.array(
    #       [[10.,   0.,   0.],
    #        [0.,  10.,   0.],
    #        [0.,   0.,  10.]]),
    #           positions=np.array(
    #       [[0.13288865,  0.13288865,  0.13288865],
    #        [-0.03325795, -0.03325795,  1.13361278],
    #        [-0.03325795,  1.13361278, -0.03325795],
    #        [1.13361278, -0.03325795, -0.03325795]])),
    #       gas=True,
    #       janaf_file='H-083.txt',
    #       name='NH3gas')

    # C2H4gas = Species(Atoms(),
    #                   gas=True,
    #                   janaf_file='C-128.txt',
    #                   name='C2H4gas')

    # HClgas = Species(Atoms(),
    #                  gas=True,
    #                  janaf_file='Cl-026.txt',
    #                  name='HClgas')

    # Cl2gas = Species(Atoms(),
    #                  gas=True,
    #                  janaf_file='Cl-073.txt',
    #                  name='Cl2gas',)

    # H2Ogas = None
    H2Ogas = Species(
        molecule('H2O'),
        gas=True,
        janaf_file=os.path.join(os.path.dirname(__file__), 'extensions/janaf_data/H-064.txt'),
        name='H2Ogas',
        frequencies=[3657, 1595, 3756],
        geometry='nonlinear',
        symmetrynumber=2,
        spin=0)

    # H2Oliquid = Species(Atoms(),
    #                     gas=False,
    #                     janaf_file='H-063.txt',
    #                     name='H2Oliquid',)

    # CH3CHOgas = Species(molecule('CH3CHO'),
    #     gas=True,
    #     name='CH3CHOgas',
    #     frequencies=[3014,2923,2716,1743,1433,1395,1352,1114,867,509,2964,1431,1102,764,150],
    #     geometry='nonlinear',
    #     symmetrynumber=1,
    #     spin=0)

    # CH3CH2OHgas = Species(molecule('CH3CH2OH'),
    #     gas=True,
    #     name='CH3CH2OHgas',
    #     frequencies=[3653,2984,2939,2900,1490,1464,1412,1371,1256,1091,1028,888,417,
    #                  2991,2910,1446,1275,1161,812],
    #     geometry='nonlinear',
    #     symmetrynumber=1,
    #     spin=0)
