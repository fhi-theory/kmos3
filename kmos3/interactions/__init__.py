#!/usr/bin/env python3
"""
The interactions module is for using in kmos3 build files
It relies upon several sub-modules that enable the following:

a) Defining nearest neighbors out to specified distance
b) Defining pairwise interactions
c) Defining activation energies based on BEP relations with the pairwise interactions.
"""


from kmos3.interactions.configurationsAndInteractions import *