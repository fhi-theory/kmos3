#!/usr/bin/env python3

import os
import sys


def test_coord_comparison(move_to_directory):
    sys.path.insert(0,
        os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            '..',
            '..',
            'examples'
    ))

    import AB_model
    # print(dir(AB_model))
    kmc_model = AB_model.main()

    coord1 = kmc_model.layer_list.generate_coord('a.(1,0,0)')
    coord2 = kmc_model.layer_list.generate_coord('a.(1,0,0)')

    assert coord1==coord2
    assert (coord1!=coord2) == False
