#!/usr/bin/env python3

import filecmp
import os
import kmos3
TEST_DIR = 'test'
REFERENCE_DIR = 'reference'


def skip_dtd_version(line):
    if 'kmc version' in line:
        return False
    if 'connected_variables_string' in line:
        return False
    return True

def test_xml_ini_conversion(move_to_directory):
    kmc_model = kmos3.create_kmc_model()
    kmc_model.import_file('reference/AB_model.xml')
    kmc_model.save('test/AB_model.ini')
    # assert filecmp.cmp('test/AB_model.ini', 'reference/AB_model.ini')
    with open('reference/AB_model.ini') as f1, open('test/AB_model.ini') as f2:
        f1 = filter(skip_dtd_version, f1)
        f2 = filter(skip_dtd_version, f2)
        assert all(x == y for x, y in zip(f1, f2))
    os.remove('test/AB_model.ini')

def test_ini_xml_conversion(move_to_directory):
    kmc_model = kmos3.create_kmc_model()
    kmc_model.import_file('reference/AB_model.ini')
    kmc_model.save('test/AB_model.xml')
    # assert filecmp.cmp('test/AB_model.xml', 'reference/AB_model.xml')
    with open('reference/AB_model.xml') as f1, open('test/AB_model.xml') as f2:
        f1 = filter(skip_dtd_version, f1)
        f2 = filter(skip_dtd_version, f2)
        assert all(x == y for x, y in zip(f1, f2))
    os.remove('test/AB_model.xml')
