#!/usr/bin/env python3

import kmos3
from glob import glob


def test_ini_import(move_to_directory):
    for ini_filename in glob('*.ini'):
        kmc_model = kmos3.create_kmc_model()
        kmc_model.import_file(ini_filename)
        assert kmc_model._get_ini_string()
