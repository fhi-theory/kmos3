#!/usr/bin/env python3

import kmos3
from glob import glob

# MD The evaluate_template function is mainly used to process files in
# the fortan_src directory. Therefore, the ini string
# representation is tested if it is altered.
def test_advanced_templating(move_to_directory):
    for ini_filename in glob('*.ini'):
        kmc_model = kmos3.create_kmc_model()
        kmc_model.import_file(ini_filename)
        ini_str = kmc_model._get_ini_string()
        template_str = kmos3.utils.evaluate_template(ini_str, escape_python=True)
        assert ini_str
        assert template_str
        assert ini_str == template_str
