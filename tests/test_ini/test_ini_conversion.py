#!/usr/bin/env python3

import kmos3
from glob import glob


def test_ini_conversion(move_to_directory):
    for xml_filename in glob('*.xml'):
        kmc_model = kmos3.create_kmc_model()
        kmc_model.import_file(xml_filename)
        assert kmc_model._get_ini_string()
