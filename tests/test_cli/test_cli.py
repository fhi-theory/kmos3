#!/usr/bin/env python3

import os
import pytest
import sys
import shutil
# Importing matplotlib and selecting the TkAgg backend here is
# necessary to avoid errors in the automated GitLab tests
# complaining about the automatically selected headles backend
import matplotlib
matplotlib.use('TkAgg')
import kmos3.cli


MODEL_NAME = 'test_cli'

def generate_model():
    # import kmos3
    from kmos3.types import \
        ConditionAction, \
        Coord, \
        Layer, \
        Parameter, \
        Process,\
        Project,\
        Site, \
        Species

    project = Project()

    # set meta information
    project.meta.author = 'Max J. Hoffmann'
    project.meta.email = 'mjhoffmann@gmail.com'
    project.meta.model_dimension = '2'
    project.meta.debug = 0
    project.meta.model_name = MODEL_NAME

    # add layer
    project.add_layer(Layer(name='default', sites=[
        Site(name='cus', pos='0 0.5 0.5')]))

    project.layer_list.default_layer = 'default'

    # add species
    project.add_species(Species(name='CO', color='#000000'))
    project.add_species(Species(name='empty', color='#ffffff'))
    project.species_list.default_species = 'empty'

    # add parameters
    project.add_parameter(Parameter(name='p_CO', value=0.2, scale='log'))
    project.add_parameter(Parameter(name='T', value=500, adjustable=True))
    project.add_parameter(Parameter(name='p_O2', value=1.0, adjustable=True))

    # add processes
    cus = Coord(name='cus', layer='default')
    p = Process(name='CO_adsorption', rate_constant='1000.')
    p.add_condition(ConditionAction(species='empty', coord=cus))
    p.add_action(ConditionAction(species='CO', coord=cus))
    project.add_process(p)

    p = Process(name='CO_desorption', rate_constant='1000.')
    p.add_condition(ConditionAction(species='CO', coord=cus))
    p.add_action(ConditionAction(species='empty', coord=cus))
    project.add_process(p)
    return project

@pytest.fixture(scope='function')
def setup_test(move_to_directory):
    if not os.path.exists('test_cli_local_smart/src'):
        os.makedirs('test_cli_local_smart/src')
    if not os.path.isfile('test_cli.xml'):
        from kmos3.io import export_xml
        export_xml(generate_model(), 'test_cli.xml')

@pytest.fixture(scope='function')
def setup_path(move_to_directory):
    model_path = os.path.abspath('test_cli_local_smart')
    if not model_path:
        assert False
    os.chdir(model_path)
    if not model_path in sys.path:
        sys.path.insert(0, model_path)

# This can be used to test different input options
# def get_parser_options(positional):
#     import argparse
#     parser = kmos3.cli._parser()
#     return [o for o in parser._subparsers._group_actions[0].choices[positional]._actions if
#             not (isinstance(o, argparse._HelpAction) or isinstance(o, argparse._VersionAction))]

def test_export(setup_test):
    positional = 'export'
    # options = get_parser_options(positional)
    args = [positional]
    args.append('test_cli.xml')
    args.append('--outdir=test_cli_local_smart')
    args.append('--overwrite')
    kmos3.cli.main(args)

def test_build(setup_test):
    positional = 'build'
    # options = get_parser_options(positional)
    args = [positional]
    args.append('--overwrite')
    os.chdir('test_cli_local_smart/src')
    kmos3.cli.main(args)

def test_settings_export(setup_test):
    positional = 'settings-export'
    # options = get_parser_options(positional)
    args = [positional]
    args.append('test_cli.xml')
    args.append('--outdir=test_cli_local_smart')
    kmos3.cli.main(args)

def test_rebuild(setup_path):
    positional = 'rebuild'
    # options = get_parser_options(positional)
    args = [positional]
    kmos3.cli.main(args)

def test_benchmark(setup_path):
    positional = 'benchmark'
    # options = get_parser_options(positional)
    args = [positional]
    kmos3.cli.main(args)

def test_view(setup_path):
    from unittest import mock
    import tkinter
    positional = 'view'
    # options = get_parser_options(positional)
    args = [positional]
    class fakeTk(tkinter.Tk):
        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.withdraw()
        def mainloop(self):
            self.dooneevent()
    with mock.patch('tkinter.Tk', new=fakeTk):
        kmos3.cli.main(args)

def test_import(setup_test):
    from unittest import mock
    positional = 'import'
    # options = get_parser_options(positional)
    args = [positional]
    args.append('test_cli.xml')
    with mock.patch('builtins.input', return_value='exit'):
        kmos3.cli.main(args)

def test_shell(setup_path):
    from unittest import mock
    positional = 'shell'
    # options = get_parser_options(positional)
    args = [positional]
    with mock.patch('builtins.input', return_value='exit'):
        kmos3.cli.main(args)

def test_xml(setup_path):
    from unittest import mock
    positional = 'xml'
    # options = get_parser_options(positional)
    args = [positional]
    kmos3.cli.main(args)

@pytest.fixture(scope="session", autouse=True)
def cleanup(request):
    """Cleanup a testing directory once we are finished."""
    def remove_test_dir():
        test_path = os.path.abspath(os.path.dirname(__file__))
        to_remove = os.listdir(test_path)
        to_remove.pop(to_remove.index(os.path.split(__file__)[-1]))
        to_remove.pop(to_remove.index('deprecated'))
        for i in to_remove:
            i = os.path.join(test_path, i)
            if os.path.isfile(i):
                os.remove(i)
            elif os.path.isdir(i):
                shutil.rmtree(i)
            else:
                raise RuntimeError
    request.addfinalizer(remove_test_dir)
