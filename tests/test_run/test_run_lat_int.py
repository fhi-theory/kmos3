#!/usr/bin/env python3


def test_lat_int(move_to_directory):
    import sys
    import os
    import shutil
    import pprint
    import numpy as np
    import kmos3.cli
    expected_result = (5,200)
    kmos3.cli.main('export AB_model.ini --outdir _tmp_export_lat_int -o -b lat_int')
    sys.path.insert(0, os.path.abspath(os.curdir))
    # TODO tolerances too large
    # rtol = 0.10
    rtol = 0.4
    atol = 1e-8
    from kmos3.run import KMC_Model
    with KMC_Model(print_rates=False, banner=False) as model:
        procs_sites = []
        for i in range(10000):
            proc, site = model.get_next_kmc_step()
            procs_sites.append((proc.real, site.real))
            model.run_proc_nr(proc, site)
        result = np.mean(np.array(procs_sites), axis=0)
        with open('../test_procs_sites_lat_int.log', 'w') as outfile:
            outfile.write(pprint.pformat(procs_sites))
    assert all([np.isclose(a, b, rtol=rtol, atol=atol) for a, b in zip(result, expected_result)])
    os.chdir('..')
    os.remove('abbreviations_AB_no_diffusion.dat')
    shutil.rmtree('_tmp_export_lat_int')
    os.remove('test_procs_sites_lat_int.log')
