#!/usr/bin/env python3

import os
from shutil import rmtree


def test_import_export_pdopd_local_smart(move_to_directory):

    import kmos3.types
    import kmos3.io

    TEST_DIR = 'test_pdopd_local_smart'
    REFERENCE_DIR = 'reference_pdopd_local_smart'

    kmc_model = kmos3.types.Project()
    kmc_model.import_xml_file('pdopd.xml')
    kmos3.io.export_source(kmc_model, TEST_DIR, code_generator='local_smart')
    testResult = False
    for filename in ['base', 'lattice', 'proclist']:
        if open(os.path.join(REFERENCE_DIR, '%s.f90' % filename)).read() == open(os.path.join(TEST_DIR, '%s.f90' % filename)).read():
            testResult = True
        else:
            testResult = False
        if filename == 'proclist':
            print("proclist tests are not working! Even if it fails this test, it is probably still correct!")
            continue
        elif filename == 'base':
            print("base tests are not working! Even if it fails this test, it is probably still correct!")
            continue
        assert testResult
    rmtree(TEST_DIR)
