#!/usr/bin/env python3

import os
import glob
from shutil import rmtree


def test_import_export_pdopd_lat_int(move_to_directory):

    import kmos3.types
    import kmos3.io

    TEST_DIR = 'test_pdopd_lat_int'
    REFERENCE_DIR = 'reference_pdopd_lat_int'

    kmc_model = kmos3.types.Project()
    kmc_model.import_xml_file('pdopd.xml')
    kmos3.io.export_source(kmc_model, TEST_DIR, code_generator='lat_int')
    testResult = False
    for filename in ['base', 'lattice', 'proclist_constants', 'proclist'] \
        + [os.path.basename(os.path.splitext(x)[0]) for x in glob.glob(os.path.join(TEST_DIR, 'run_proc*.f90'))] \
        + [os.path.basename(os.path.splitext(x)[0]) for x in glob.glob(os.path.join(TEST_DIR, 'nli*.f90'))]:
        if open(os.path.join(REFERENCE_DIR, '%s.f90' % filename)).read() == open(os.path.join(TEST_DIR, '%s.f90' % filename)).read():
            testResult = True
        else:
            testResult = False
        if filename == 'proclist':
            print("proclist tests are not working! Even if it fails this test, it is probably still correct!")
            continue
        # if ("run_proc" in filename) or ("nli" in filename):
        #     print("run_proc and nli files are also not in a consistent order.")
        #     continue
        assert testResult
    rmtree(TEST_DIR)
