#!/usr/bin/env python3

import os
import glob
from shutil import rmtree


def test_import_export_intZGB_otf(move_to_directory):

    import kmos3.types
    import kmos3.io

    TEST_DIR = 'test_export_intZGB_otf'
    REFERENCE_DIR = 'reference_export_intZGB_otf'

    kmc_model = kmos3.types.Project()
    kmc_model.import_xml_file('intZGB_otf.xml')
    kmos3.io.export_source(kmc_model, TEST_DIR, code_generator='otf')
    testResult = False
    for filename in ['base', 'lattice', 'proclist_pars','proclist_constants', 'proclist'] \
        + [os.path.basename(os.path.splitext(x)[0]) for x in glob.glob(os.path.join(TEST_DIR, 'run_proc*.f90'))]:
        if open(os.path.join(REFERENCE_DIR, '%s.f90' % filename)).read() == open(os.path.join(TEST_DIR, '%s.f90' % filename)).read():
            testResult = True
        else:
            testResult = False
        if filename == 'proclist':
            print("proclist tests are not working! Even if it fails this test, it is probably still correct!")
            continue
        elif "run_proc" in filename:
            print("run_proc and nli files are also not in a consistent order.")
            continue
        elif filename == 'base':
            print("base tests are not working! Even if it fails this test, it is probably still correct!")
            continue
        assert testResult
    rmtree(TEST_DIR)
