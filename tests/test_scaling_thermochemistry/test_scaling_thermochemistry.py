#!/usr/bin/env python3

import os
import subprocess
import sys
import numpy as np
import shutil
import pytest


rtol = 0.20
atol = 1e-7
expected_result = [0.0016, 0.0016]

def test_scaling_thermochemistry(move_to_directory):
    import kmos3
    subprocess.call(['python3', os.path.join('Methanation_JCP2017.py')])
    kmos3.cli.main('export Methanation_JCP2017.xml -o --outdir methanation_acc -t')
    sys.path.append(os.path.abspath(os.curdir))
    import kmos3.snapshots_globals as sg
    import kmos3.snapshots
    sg.model.set_buffer_parameter(1)
    sg.model.set_sampling_steps(20)
    sg.model.set_threshold_parameter(0.2)
    sg.model.set_execution_steps(200)
    sps = 1000000
    n_snapshots = 10
    sg.model.parameters.E_C=1.65 # Point A in JCP 2017
    sg.model.parameters.E_O=-0.6 # Point A in JCP 2017
    sg.write_output = 'False'
    kmos3.snapshots.create_headers()
    kmos3.snapshots.do_snapshots(n_snapshots, sps, acc=True)
    sps = 10000000
    n_snapshots = 5
    kmos3.snapshots.do_snapshots(n_snapshots, sps, acc=True)
    result = sg.TOF_data_list
    os.chdir('..')
    os.remove('abbreviations_Methanation_JCP2017.dat')
    os.remove('Methanation_JCP2017.xml')
    shutil.rmtree('methanation_acc')
    assert all([np.isclose(a, b, rtol=rtol, atol=atol) for a, b in zip(result, expected_result)])
