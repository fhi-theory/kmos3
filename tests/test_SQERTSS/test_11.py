#!/usr/bin/env python3

import os
import sys
import numpy as np
from copy import deepcopy
import export_import_library as eil


def test(move_to_directory):
    os.chdir('throttling_test_reaction_local_smart')
    sys.path.insert(0, os.path.abspath('.'))
    import kmos3.snapshots_globals as sg
    import kmos3.snapshots as snapshots
    import kmos3.throttling_globals as tg
    import kmos3.throttling as throttling
    tg.FFP_roof = None
    # File names for loading/saving parameters
    tg_load_file = '../test_throttle_case_11_params.txt'
    tg_save_file = '../test_throttle_case_11_params_out.txt'

    # Module object for saving/loading
    tg_module = eil.module_export_import(tg_save_file, tg_load_file, tg)

    # Load the module
    tg_module.load_params()

    # Make sure to update the new ATF dictionary so we don't accidentally get
    # one with the wrong size or process entries. (The ITF one is automatically
    # created.)
    tg.aggregate_throttling_factors_dict = deepcopy(
        tg.aggregate_throttling_factors_dict_old)

    tg.aggregate_throttling_factors

    # Calculate throttling factors
    throttling.calculate_throttling_factors(unthrottle_slow_processes=False)

    # Save the module
    tg_module.save_params()

    tg.aggregate_throttling_factors_dict

    #Now to compare the expected and actual.

    #Here is an expectedResultsDict that is being populated manually using the excel sheet 161220_ManualExamples_Deprecated.xlsx (see near column AI).
    #The format inside here is reaction number and ptEF, where the ptEF is the higher of the forward and reverse reaction after throttling.
    #In the test files above, the aggregate_throttling_factors are not populated. But tg.ptEF_list is, so we'll use ptEF_list.  
    expected_result = \
    [
        [	4	,	0.129855187	]	,
        [	3	,	0.129855187	]	,
        [	1	,	0.259710375	]	,
        [	2	,	0.324637969	]	,
        [	5	,	20.7119024	]	,
        [	6	,	207.119024	]	,
        [	7	,	2071.19024	]	,
    ]
    expected_result = eil.getSortedExpected_ptEF_list(expected_result)
    result = eil.getSortedActual_ptEF_list(tg.ptEF_list)

    rtol = 1.0e-5
    atol = 1.0e-8
    assert np.isclose(expected_result, result, rtol=rtol, atol=atol).all()
    os.remove(tg_save_file)
