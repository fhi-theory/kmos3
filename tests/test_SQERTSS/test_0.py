#!/usr/bin/env python3

import os
import subprocess


def test_xml(move_to_directory):
    import kmos3
    subprocess.call(['python3', 'throttling_test_reaction.py'])
    kmos3.cli.main('export throttling_test_reaction.xml -o -b local_smart')
    # This file is really just to export the xml before the other unit tests are run.
    # We will keep and use the expected xml in order to make this a unit test.
    with open("../throttling_test_reaction_expected_xml.xml", "r") as f:
        expected_result = f.read()
    with open("../throttling_test_reaction.xml", "r") as f:
        result = f.read()
    assert expected_result == result
    os.remove("../abbreviations_throttling_test_reaction.dat")
