import ast # Library for parsing string representations of Python objects

# Class to encapsulate each module and the associated methods for storing it on
# disk.
class module_export_import:

    def __init__(self, save_filename, load_filename, module):
        self.save_filename = save_filename
        self.load_filename = load_filename
        self.module = module

    # This function is for saving all of the variables in the specified module.
    def save_params(self):

        # Get a list of all the exportable variables in the module. These
        # variables are listed in the special __var_list__ variable in the
        # module.
        module_vars = getattr(self.module, '__var_list__')

        # Loop over all variables in the module, get each value, and write it to
        # disk if it is a simple object (elementary data type or one-deep
        # container of elementary data types).
        with open(self.save_filename, 'wt') as f:
            for module_var in module_vars:
                module_var_val = getattr(self.module, module_var)
                if (type(module_var_val) == type('str')) or (type(module_var_val) == type(b'str')):
                    # We need to make sure string values get printed as string
                    # values or they won't be read properly on reload
                    f.write(module_var + " = '" + str(module_var_val) + "'\n")
                else:
                    # This a non-string elementary data type, so we write it as-is
                    f.write(module_var + ' = ' + str(module_var_val) + '\n')

    # This function is for setting variables in the specified module using
    # previously saved parameters.
    def load_params(self):

        # Loop over all variables in the file, get each value, and save it to
        # the module.
        with open(self.load_filename, 'rt') as f:
            for line in f:

                # This checks for comment and blank lines and skips them
                if line.strip().startswith('#') or len(line.strip()) == 0:
                    continue

                # NOTE: Alternatively, if we didn't care about security, we
                # could just exec each line (or the whole file) as the lines are
                # written in valid Python syntax of 'variable = value'.

                # Split on first equal sign only and strip surrounding
                # whitespace.  Stripping the whitespace is necessary or else we
                # will have a variable 'name' or 'value' that is surrounded by
                # spaces. When the 'name' is actually ' name ' (note the
                # spaces), setattr() will assign the parsed value to a module
                # variable named ' name ', not 'name'. If the value has
                # surrounding whitespace (at least leading whitespace), the ast
                # parser will give an 'unexpected indent' error.
                module_var, module_var_val = line.split('=', 1)
                module_var = module_var.strip()
                module_var_val = module_var_val.strip()

                # Convert to an actual Python object
                # Workaround for sets as literal_eval can't handle them in
                # Python 2.7
                if 'set' in module_var_val:
                    # Extract the bracketed portion which can be converted into
                    # a list
                    module_var_val = module_var_val[4:-1]

                    # Convert to a list
                    module_var_val = ast.literal_eval(module_var_val)

                    # Convert list to a set
                    module_var_val = set(module_var_val)
                else:
                    module_var_val = ast.literal_eval(module_var_val)

                # Store it in the module
                setattr(self.module, module_var, module_var_val)

#Takes one expected_ptEFs_List at a time, like expectedResultsDict['19'], and then returns a sorted version.
def getSortedExpected_ptEF_list(expected_ptEFs_List):
    sortedExpected_ptEF_list = []
    for reactionNumberDesired in range(1,len(expected_ptEFs_List)+1): #loop once for each reaction to add
        for listIndex in range(0,len(expected_ptEFs_List)): #loop again for each entry.
            currentReactionNumber = expected_ptEFs_List[listIndex][0]
            if currentReactionNumber == reactionNumberDesired:
                sortedExpected_ptEF_list.append(expected_ptEFs_List[listIndex])
    return sortedExpected_ptEF_list

#In the test files, the aggregate_throttling_factors are not populated. But tg.ptEF_list is, so we'll use ptEF_list.  One difference in the test files relative to the excel sheet is that ptEF_list has None objects instead of zeros. #So we can't use an == check without first converting any "None" into zero.  We actually don't need to check whether the reaction is forward or reverse because one of the items in the ptEF_list is the greater of the two EF's.
#The individual elements in the ptEF_List are like this:
#['1p0', 0.11, 0.11, 1.0, 0.11, 'F', 0]
#[RxnString, F_EF, R_EF, ITF??, faster_EF, directionOf_faster_EF, RxnIndex]
#So we just need to pull out (in arrayIndexing) index 4 and 6. We do this with a helper function called get_ptEF_for_rxn.
def get_Actual_ptEF_for_rxn(full_ptEF_List, desiredRxnNumber): #feed tg.ptEF_List as full_ptEF_List
    for rxn_ptEF_list in full_ptEF_List: #The tg.ptEF_List is actually a list of lists. rxn_ptEF_list is an individual reaction's ptEF_list.
        if rxn_ptEF_list[6] + 1  == desiredRxnNumber: #This checks if it's the right reaction number.
            ptEF_for_rxn = rxn_ptEF_list[4] #this gets the ptEF for that reaction.
            if type(ptEF_for_rxn) == type(None): #set to 0 if it's a None type.
                ptEF_for_rxn = 0
            return ptEF_for_rxn
       #else pass #is implied.

def getSortedActual_ptEF_list(full_ptEF_List):
    sortedActual_ptEF_list = []
    for reactionNumberDesired in range(1,len(full_ptEF_List)+1): #loop once for each reaction to add
        Actual_ptEF_for_rxn = get_Actual_ptEF_for_rxn(full_ptEF_List, reactionNumberDesired)
        sortedActual_ptEF_list.append([reactionNumberDesired, Actual_ptEF_for_rxn])
    return sortedActual_ptEF_list
