#!/usr/bin/env python3

import sys
import os
import shutil


def build_model():
    import subprocess
    import kmos3.cli
    subprocess.call(['python3', 'MyFirstThrottling__build.py'])
    kmos3.cli.main('export MyFirstThrottling.xml -o')

expected_result = [
    -0.738173642, -0.742972524, -0.692893661, -0.636517194, 1.273828757, 1.914869151, 1.995825644]

def test(move_to_directory):
    build_model()
    sys.path.insert(0, os.path.abspath(os.curdir))
    import kmos3.snapshots_globals as sg
    import kmos3.snapshots as snapshots
    import kmos3.throttling_globals as tg
    import kmos3.throttling as throttling
    import ast

    # Class to encapsulate each module and the associated methods for storing it on disk.
    class module_export_import:

        def __init__(self, save_filename, load_filename, module):
            self.save_filename = save_filename
            self.load_filename = load_filename
            self.module = module

        # This function is for saving all of the variables in the specified module.
        def save_params(self):

            # Get a list of all the exportable variables in the module. These
            # variables are listed in the special __var_list__ variable in the
            # module.
            module_vars = getattr(self.module, '__var_list__')

            # Loop over all variables in the module, get each value, and write it to
            # disk if it is a simple object (elementary data type or one-deep
            # container of elementary data types).
            with open(self.save_filename, 'wt') as f:
                for module_var in module_vars:
                    module_var_val = getattr(self.module, module_var)
                    if (type(module_var_val) == type('str')) or (type(module_var_val) == type(b'str')):
                        # We need to make sure string values get printed as string
                        # values or they won't be read properly on reload
                        f.write(module_var + " = '" + str(module_var_val) + "'\n")
                    else:
                        # This a non-string elementary data type, so we write it as-is
                        f.write(module_var + ' = ' + str(module_var_val) + '\n')

        # This function is for setting variables in the specified module using
        # previously saved parameters.
        def load_params(self):

            # Loop over all variables in the file, get each value, and save it to
            # the module.
            with open(self.load_filename, 'rt') as f:
                for line in f:

                    # This checks for comment and blank lines and skips them
                    if line.strip().startswith('#') or len(line.strip()) == 0:
                        continue

                    # NOTE: Alternatively, if we didn't care about security, we
                    # could just exec each line (or the whole file) as the lines are
                    # written in valid Python syntax of 'variable = value'.

                    # Split on first equal sign only and strip surrounding
                    # whitespace.  Stripping the whitespace is necessary or else we
                    # will have a variable 'name' or 'value' that is surrounded by
                    # spaces. When the 'name' is actually ' name ' (note the
                    # spaces), setattr() will assign the parsed value to a module
                    # variable named ' name ', not 'name'. If the value has
                    # surrounding whitespace (at least leading whitespace), the ast
                    # parser will give an 'unexpected indent' error.
                    module_var, module_var_val = line.split('=', 1)
                    module_var = module_var.strip()
                    module_var_val = module_var_val.strip()

                    # Convert to an actual Python object
                    # Workaround for sets as literal_eval can't handle them in
                    # Python 2.7
                    if 'set' in module_var_val:
                        # Extract the bracketed portion which can be converted into
                        # a list
                        module_var_val = module_var_val[4:-1]

                        # Convert to a list
                        module_var_val = ast.literal_eval(module_var_val)

                        # Convert list to a set
                        module_var_val = set(module_var_val)
                    else:
                        module_var_val = ast.literal_eval(module_var_val)

                    # Store it in the module
                    setattr(self.module, module_var, module_var_val)

    def set_PRNG_state(load_saved_parameters_on, random_seed=None):

        if load_saved_parameters_on:
            snapshots.seed_PRNG(restart=True, state=sg.PRNG_state)
        else:
            snapshots.seed_PRNG(restart=False, state=random_seed)

    # Initialize variables
    # File names for loading/saving parameters (optional, but useful).
    tg_load_file = 'test_reaction_throttling_parameters.txt'
    tg_save_file = 'test_reaction_throttling_parameters.txt'
    sg_load_file = 'test_reaction_snapshots_parameters.txt'
    sg_save_file = 'test_reaction_snapshots_parameters.txt'
    tg_eil_object = module_export_import(tg_save_file, tg_load_file, tg)
    sg_eil_object = module_export_import(sg_save_file, sg_load_file, sg)
    eic_module_objects = [tg_eil_object, sg_eil_object]

    sg.model.parameters.T = 600

    #If you want to set the random seed:
    random_seed = -731543675
    set_PRNG_state(load_saved_parameters_on=True, random_seed=random_seed)

    # The number of snapshots
    Nsnapshots = 50 # Number of snapshots to run

    # The total steps per snapshot (sps) and time per snapshot (tps). Values of None
    # mean to use tg.throttling_sps and tg.throttling_tps instead.
    sps = None
    tps = None
    tg.FFP_roof = 1000 #NOTE: For this example, this setting is important.

    # Write output file headers if output on and we have not loaded saved data (this is before running simulation)
    snapshots.create_headers()

    # Execute throttled snapshots
    # by default, settings will be exported with the eic_module after each set/batch of throttled snapshots,
    # as long as eic_module_objects is provided.
    throttling.do_throttled_snapshots(Nsnapshots, sps=sps, tps=tps, eic_module_objects=eic_module_objects)
    #one can then do other analysis and call do_throttled_snapshots again.

    # Write summary/diagnostic data (this is after running simulation)
    snapshots.create_log()

    import numpy as np
    MyFirstThrottling_EFs_and_Coverages = np.genfromtxt("MyFirstThrottling_TOFs_and_Coverages.csv", skip_header=1, dtype='float', delimiter=",")
    snapshot_40_data = MyFirstThrottling_EFs_and_Coverages[40]
    snapshot_40_data_ForwardEventFrequencies = snapshot_40_data[7:13+1] 
    log10_snapshot_40_data_ForwardEventFrequencies = np.log10(snapshot_40_data_ForwardEventFrequencies)
    actualResult = log10_snapshot_40_data_ForwardEventFrequencies

    # print(expected_result)
    # print(actualResult)

    """We set our tolerances. There can be some rounding when the tolerances get checked, so they are not exact."""
    # The relative tolerances have to be quite large this time due to statistical fluctuations.
    # It would probably pass most of the time with 0.10, but 0.20 is more safe and is still very distinct for what comes out with this algorithm.
    rtol = 0.20 
    atol = 1e-5
    os.chdir('..')
    os.remove('abbreviations_MyFirstThrottling.dat')
    os.remove('MyFirstThrottling.xml')
    shutil.rmtree('MyFirstThrottling_local_smart')
    assert all(np.isclose(expected_result, actualResult, rtol=rtol, atol=atol))
