#!/usr/bin/env python3

import os
import subprocess
import sys
import numpy as np
import shutil
import pytest


def build_model():
    import kmos3.cli
    kmos3.cli.main('export CO_oxidation_Ruo2.xml -o --outdir non_accelerated_test')

rtol = 0.40
atol = 2e-7
expected_result = [3.4e-7]

def test_non_acc(move_to_directory):
    subprocess.call(['python3', 'CO_oxidation_Ruo2.py'])
    build_model()
    sys.path.insert(0, os.path.abspath(os.curdir))
    import kmos3.snapshots_globals as sg
    import kmos3.snapshots
    sg.model.parameters.p_COgas = 1e-7
    sg.model.parameters.p_O2gas = 1e-10
    sg.model.parameters.T = 350
    kmos3.snapshots.create_headers()
    kmos3.snapshots.do_snapshots(5, 3000000)
    kmos3.snapshots.do_snapshots(1, 3000000*5)
    result = sg.TOF_data_list
    os.chdir('..')
    os.remove('abbreviations_CO_oxidation_Ruo2.dat')
    os.remove('CO_oxidation_Ruo2.xml')
    assert all([np.isclose(a, b, rtol=rtol, atol=atol) for a, b in zip(result, expected_result)])
