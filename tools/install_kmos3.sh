#!/usr/bin/env bash


echo "Simple script to automatically install kmos3 in \$HOME/kmos3"\
     "within the APT system of Debian-based Linux distributions"

MYPWD=$PWD
while true; do
    read -p "For the installation root priviliges are required. Do you wish to continue? [y/N] " yn
    case $yn in
        [Yy] )
            {
                (
                    sudo apt-get update;
                    sudo apt-get install git gfortran python3-dev python3-venv;;
                    cd $HOME;
                    git clone https://gitlab.mpcdf.mpg.de/fhi-theory/kmos3.git;
                    cd kmos3;
                    python3 -m venv $HOME/kmos3_venv;
                    source $HOME/kmos3_venv/bin/activate;
                    pip install .;
                    cd examples;
                    python3 ZGB_model__build.py;
                    cd zgb_model_local_smart;
                    kmos3 benchmark 1> /dev/null;
                    cd $MYPWD;
                )
            } || {
                cd $MYPWD;
            };
            break;;
        [Nn]|"" )
            break;;
        * )
            echo "Please answer y (yes) or n (no).";;
    esac
done
