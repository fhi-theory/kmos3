#!/usr/bin/env bash


echo "Simple script to automatically install the kmos3 dependencies"\
     "within the APT system of Debian-based Linux distributions"

while true; do
    read -p "For the installation root priviliges are required. Do you wish to continue? [y/N] " yn
    case $yn in
        [Yy] )
            sudo apt-get update;
            sudo apt-get install gfortran python3-dev python3-venv;
            break;;
        [Nn]|"" )
            break;;
        * )
            echo "Please answer y (yes) or n (no).";;
    esac
done
