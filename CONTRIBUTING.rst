.. note::
  The source code of kmos3 is maintained on a GitLab run by the
  `Max Planck Computing and Data Facility (MPCDF) <https://www.mpcdf.mpg.de/>`_
  which requires an account in order to contribute yourself.

Contributions of any sort are incredibly valuable and we highly encourage contributing to the
kmos3 source code. If you have any questions, found a bug, or have suggestions for new features
you can either

- send us an email to kmos3.at.fhi.mpg

or

- open issues on the GitLab.

GitLab Access
=============

Gaining access to the MPCDF GitLab is simple. Just send us an email to kmos3.at.fhi.mpg with your
name and an email address. After accepting the MPCDF terms and conditions and logging into the
GitLab once, we can add the account to the repository and you will be able to open issues and push
your code.

Issues
======

Please keep issues short and use descriptive titles. Provide us with a short description of your
request as well as the use case and if applicable state the versions of the software and
dependencies. This gives us the opportunity to reproduce the problem.

Documentation
=============

The `documentation <https://kmos3.org/doc/>`_ is part of the kmos3 source code and contributing to
it works the same way as with kmos3 itself. Currently, we are working on its revision and updates
will be published as soon as possible.

Branches
========

`main` (protected, default): Stable release

`develop` (protected): Latest features and fixes

`feature_*`: Create a feature branch and put your work here

Tags
====

Tags for (pre-)releases are of the form X.X.X(rcX).

Commits
=======

Before committing always check the code style, lint, and try to keep a clean commit history. To
this end, have a look at 
`Google's Python style guide <https://google.github.io/styleguide/pyguide.html>`_
and use pep8 as well as pylint. In the future, we plan to automatize this task on the repository
level.

In the usual case where only one person is working on a feature branch, rebases, amendments, and
force pushes are allowed.

Merges
======

First create an issue and work on it on a dedicated feature branch, which is based on the develop
branch. As soon as you are done run the tests locally. Think about the option to squash certain
commits for a more streamlined commit history.

Only after passing the tests, rebase your feature branch with develop and then create the merge
request with the target develop in the GitLab UI using the "Create merge request" button within
the issue.

The final merge should then be performed by the merge request author after a positive review and
the feature branch should be deleted.

CI/CD
=====

GitLab will run a CI/CD pipeline to run the tests upon changes to the develop and main branch. If
your code passed these tests the pipeline will (usually) also succeed. In the unlikely case of
failures one needs to resolve them before requesting a review.
